Bluetooth LE Logger
===================
The BLELogger defines a device that continuously records Bluetooth LE
advertising packets to an SD card.

Dependencies
------------
The project needs build tools (for C++11), cmake (at least 3.0), git, the
Bluetooth/Bluez development library, and optionally the Systemd development
library (you really don't need systemd, although keep in mind that I didn't
create an init script for the service). The parse integration further requires
the curl development library and the uuid library. The version system uses
pkg-config. On a debian derivative, you may call:

.. code-block:: shell

    $ sudo apt-get update
    $ sudo apt-get install build-essential git cmake pkg-config ninja-build
    $ sudo apt-get install bluez libbluetooth-dev libsystemd-dev libcurl4-gnutls-dev uuid-dev

On arch linux, you can use (the other dependencies are included with the base
system):

.. code-block:: shell

    $ sudo pacman -S cmake ninja bluez bluez-libs


Installing
----------
After installing the dependencies, use the following sequence of commands to
build the service. By the way, by default it builds in Debug mode. I personally
prefer ninja as build-system, but you can use anything that's supported by
CMake.

.. code-block:: shell

    $ git clone https://bitbucket.com/eleanorey/blelogger.git
    $ mkdir -p blelogger/build
    $ cd blelogger/build
    $ cmake -GNinja -DCMAKE_BUILD_TYPE=Release ..
    $ ninja
    $ sudo ninja install

You may uninstall the service (after following the above steps) with:

.. code-block:: shell

    $ sudo ninja uninstall

Running
-------
The programme is designed to run as a service, but since it doesn't fork,
you can run it just like any other.

.. code-block:: shell

    $ sudo /usr/local/bin/blelogger

To start the service automatically, use systemd's interface. If you intend to
use this, ensure that the user ``blelogger`` is present on the system. For Parse
to work without errors, the account should have a home directory, but it does
not need login capabilities. Systemd uses capability bounding sets to enable the
proper permissions.

.. code-block:: shell

    $ sudo systemctl enable blelogger
    $ sudo systemctl start blelogger

Configuration
-------------
You can configure the blelogger either at build time or later using a configuration file, which can
be found at

.. code-block:: shell

    /etc/blelogger/blelogger.ini

Instead of using the usual cmake configure command, you may change some options, for example:

.. code-block:: shell

    $ cmake -GNinja -DENABLE_ACTIVE_SCAN=ON ..

The following options are available:

* ENABLE_TESTS: Toggle unit testing. Default ON.
* ENABLE_BLUETOOTH: Toggle calls to the bluetooth library. Default ON.
* ENABLE_PARSE: Toggle calls to the Parse API. Default ON.
* ENABLE_SYSTEMD: Toggle calls to Systemd. Default ON.
* ENABLE_FILE_WRITE: Toggle dump file writing. Default ON.
* ENABLE_STRICT_ERROR: Toggle strict error handling. Default OFF.
* ENABLE_ACTIVE_SCAN: Toggle active bluetooth scanning. Default OFF.
* ENABLE_BEACON_FILTER: Toggle the beacon filtering. Default ON.

You may also want to set the Parse configuration yourself:

* PARSE_APP_ID: The string of the Parse App ID. Ex. "myAppId"
* PARSE_SERVER_URL: The URL to the Parse server including the Parse mount. Ex. "https://localhost/parse"
* PARSE_REST_URL: The URL subpath to the Parse class. Ex. "/classes/AdInfo"
* PARSE_SESSION_TOKEN: In order for the Parse logging to work, you need a valid (restricted) Parse session token.

Or modify the user under which the service runs in Systemd:

* EXECUTABLE_USER: The username that's used to run the logger. Ex. "blelogger"

Usage
-----
Once it runs, the service collects Bluetooth LE/Smart advertisement packets
from nearby devices and sends them to a Parse server. In case something goes
wrong, the packets are stored as successive JSON strings (separated by a
newline) in the following dump file. As of yet, these packets are not resent.

.. code-block:: shell

    /etc/blelogger/blelogger.bkp

Contributing
------------
If you wish to contribute to this piece of software, please make use of the
Vagrant virtualization environment. A Vagrant file is already present and you
can use it to build and test the software. If you do not know what Vagrant is,
refer to their great documentation: https://www.vagrantup.com/docs/why-vagrant/

So, when you want to work on the software, just type the following in the
project directory:

.. code-block:: shell

    $ vagrant up
    $ vagrant reload
    $ vagrant ssh
    [vagrant] $ cd /vagrant

And then continue with the build process from there. The dependencies are
already installed as part of the Vagrant provisioning process.

Documentation
-------------
Feel free to consult the code documentation in the subdirectory ``docs/latest``. If you
want to build the documentation yourself, you will additionally need
``Sphinx`` and ``Doxygen`` (or just use the Vagrant box mentioned above).
