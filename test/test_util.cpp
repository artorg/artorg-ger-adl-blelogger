#include "gtest/gtest.h"
#include "util.h"

TEST (Utilities, ToHexDelim) {
    const std::size_t bufLen = 10;
    const std::uint8_t buf[bufLen] = {
        0x00U, 0x11U, 0x22U, 0x33U, 0x44U, 0x55U, 0x66U, 0x77U, 0xeeU, 0xffU
    };
    const char byteDelim = ':';
    std::string intendedBufString = "00:11:22:33:44:55:66:77:ee:ff";

    std::string bufString = BleLogger::Util::toHex (buf, bufLen, byteDelim);

    ASSERT_STREQ (intendedBufString.c_str (), bufString.c_str ());
}

TEST (Utilities, ToHex) {
    const std::size_t bufLen = 10;
    const std::uint8_t buf[bufLen] = {
        0x00U, 0x11U, 0x22U, 0x33U, 0x44U, 0x55U, 0x66U, 0x77U, 0xeeU, 0xffU
    };
    std::string intendedBufString = "0011223344556677eeff";

    std::string bufString = BleLogger::Util::toHex (buf, bufLen);

    ASSERT_STREQ (intendedBufString.c_str (), bufString.c_str ());
}

TEST (Utilities, ToUUID) {
    const std::size_t bufLen = 16;
    const std::uint8_t buf[bufLen] = {
        0x00U, 0x11U, 0x22U, 0x33U, 0x44U, 0x55U, 0x66U, 0x77U, 0x88U, 0x99U,
        0xAAU, 0xBBU, 0xCCU, 0xDDU, 0xEEU, 0xFFU
    };
    const char uuidDelim = '-';
    std::string intendedBufString = "00112233-4455-6677-8899-aabbccddeeff";

    std::string bufString = BleLogger::Util::toUuid (buf, bufLen, uuidDelim);

    ASSERT_STREQ (intendedBufString.c_str (), bufString.c_str ());
}

TEST (Utilities, ToUUIDInvalidLength) {
    const std::size_t bufLen = 2;
    const std::uint8_t buf[bufLen] = {
        0x00U, 0x11U
    };
    const char uuidDelim = '-';
    std::string intendedBufString = "INVALID";

    std::string bufString = BleLogger::Util::toUuid (buf, bufLen, uuidDelim);

    ASSERT_STREQ (intendedBufString.c_str (), bufString.c_str ());
}

TEST (Utilities, ToUint16) {
    for (std::uint8_t f = 0x00U; f < 0xffU; f++) {
        for (std::uint8_t s = 0x00U; s < 0xffU; s++) {
            std::uint16_t intention = f | (static_cast<std::uint16_t> (s) << 8);
            std::uint16_t result = BleLogger::Util::toUint16 (f, s);
            ASSERT_EQ (intention, result);
        }
    }
}
