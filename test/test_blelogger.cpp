#include "gtest/gtest.h"
#include "blelogger.h"

TEST (BleLoggerConstructor, ParameterLessConstructor) {
    auto logger = BleLogger::Logger ();
}

TEST (BleLoggerTest, InitialState) {
    auto logger = BleLogger::Logger ();

    ASSERT_EQ (BleLogger::State::Uninitialized, logger.getState ());
}

TEST (BleLoggerTest, HeartBeatAbortProgram) {
    BleLogger::Logger logger = BleLogger::Logger ();
    logger.enableBluetooth = false;
    logger.enableParse = false;
    logger.enableSystemd = false;

    int abortProgram = 1;
    int reloadConfig = 0;

    logger.heartBeat (abortProgram, reloadConfig);

    EXPECT_EQ (0, abortProgram) << "abortProgram should be reset to 0";
    EXPECT_EQ (0, reloadConfig) << "reloadConfig was modified when it shouldn't be";
    ASSERT_EQ (BleLogger::State::Shutdown, logger.getState ());
}

TEST (BleLoggerTest, HeartBeatReloadConfig) {
    BleLogger::Logger logger = BleLogger::Logger ();
    logger.enableBluetooth = false;
    logger.enableParse = false;
    logger.enableSystemd = false;

    int abortProgram = 0;
    int reloadConfig = 1;

    logger.heartBeat (abortProgram, reloadConfig);

    EXPECT_EQ (0, abortProgram) << "abortProgram was modified when it shouldn't be";
    EXPECT_EQ (0, reloadConfig) << "reloadConfig should be reset to 0";
    ASSERT_EQ (BleLogger::State::Reload, logger.getState ());
}

TEST (BleLoggerTest, HeartBeatUninitialized) {
    BleLogger::Logger logger = BleLogger::Logger ();
    logger.enableBluetooth = false;
    logger.enableParse = false;
    logger.enableSystemd = false;

    int abortProgram = 0;
    int reloadConfig = 0;

    logger.heartBeat (abortProgram, reloadConfig);

    EXPECT_EQ (0, abortProgram) << "abortProgram was modified when it shouldn't be";
    EXPECT_EQ (0, reloadConfig) << "reloadConfig was modified when it shouldn't be";
    ASSERT_EQ (BleLogger::State::DeviceSetup, logger.getState ());
}
