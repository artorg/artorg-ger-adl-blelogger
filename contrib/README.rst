Contributions
=============
This directory contains non-essential contributions to the project made during development. For
instance, the sub-directory arduino contains the previous blelogger implementation for the Arduino,
although it is unfinished.

Furthermore, this directory contains a UML diagram of the states used by the blelogger, and lastly,
the files that start with DAT_* hold data dumps of a distance measurement experiment with an
Estimote iBeacon and a Fitbit Charge HR.

Bluetooth LE Signal Strength Measurement
========================================
Schedule
--------
+-----------+-----------+
| Time      | Distance  |
+===========+===========+
| 0 min     | on top    |
+-----------+-----------+
| 1 min     | 0.5 m     |
+-----------+-----------+
| 2 min     | 1 m       |
+-----------+-----------+
| 3 min     | 1.5 m     |
+-----------+-----------+
| 4 min     | 2 m       |
+-----------+-----------+
| 5 min     | 2.5 m     |
+-----------+-----------+
| 6 min     | 3 m       |
+-----------+-----------+
| 7 min     | 3.5 m     |
+-----------+-----------+

At the point of recording, the Estimote iBeacon used the address af:3b:f2:59:53:dd, and the Fitbit
Charge HR used 38:46:c2:42:dc:f8. The logging device was a Raspberry Pi 3 with the following layout.

.. image:: raspi_layout.png

Part A
------
Motion perpendicular to the long side of the device with an Estimote iBeacon.  Note: Moved one stage
11s too late.

Part B
------
Motion perpendicular to the short side of the device with an Estimote iBeacon.  Note: Moved to 1m
17s too late.

Part C
------
Same as Part A, but with a Fitbit Charge HR, clock-face facing the device.

Part D
------
Same as Part B, but with a Fitbit Charge HR, clock-face facing the device.
