#pragma once

#include "hcilib/hcilib.hpp"
#include "memoryfree/memoryfree.hpp"
#include "timelib/timelib.hpp"
#include "util.hpp"

#include "mock/blelogger_mock.hpp"
#include "mock/sdfat_mock.hpp"

#include <stdint.h>

namespace Logging {
    enum class State : uint8_t {
        Uninitialized,
        SerialSetup,
        ClockSync,
        SDSetup,
        Initialized,
        DispatchGdi,
        AwaitGdiHecs,
        AwaitGdid,
        DispatchSem,
        AwaitSemCc,
        DispatchLsem,
        AwaitLsemCc,
        DispatchLssp,
        AwaitLsspCc,
        DispatchLsse,
        AwaitLsseCc,
        Scanning,
        DispatchReset,
        AwaitRCc,
        Error,
        Critical
    };

    enum class Message : uint8_t {
        None,
        SdCardNotAvailable,
        CommandNotSendable,
        CommandFailure,
        EventFailure,
        TimeoutError,
        IllegalState
    };

    static const char* getStateStr(State s) {
        switch (s) {
            case State::Uninitialized: return "Uninitialized";
            case State::SerialSetup: return "Serial setup";
            case State::ClockSync: return "Clock synchronisation";
            case State::SDSetup: return "SD setup";
            case State::Initialized: return "Initialized";
            case State::DispatchGdi: return "Dispatch GapDeviceInit";
            case State::AwaitGdiHecs: return "Await GapDeviceInit HciExtCommandStatus";
            case State::AwaitGdid: return "Await GapDeviceInitDone";
            case State::DispatchSem: return "Dispatch SetEventMask";
            case State::AwaitSemCc: return "Await SetEventMask CommandComplete";
            case State::DispatchLsem: return "Dispatch LESetEventMask";
            case State::AwaitLsemCc: return "Await LESetEventMask CommandComplete";
            case State::DispatchLssp: return "Dispatch LESetScanParameters";
            case State::AwaitLsspCc: return "Await LESetScanParameters CommandComplete";
            case State::DispatchLsse: return "Dispatch LESetScanEnable";
            case State::AwaitLsseCc: return "Await LESetScanEnable CommandComplete";
            case State::Scanning: return "Scanning";
            case State::DispatchReset: return "Dispatch Reset";
            case State::AwaitRCc: return "Await Reset CommandComplete";
            case State::Error: return "Error";
            case State::Critical: return "Critical";
            default: return "";
        }
    }

    static const char* getSateMessage(Message m) {
        switch (m) {
            case Message::None: return "None";
            case Message::SdCardNotAvailable: return "SD card not available";
            case Message::CommandNotSendable: return "Command not sendable";
            case Message::CommandFailure: return "Command failure";
            case Message::EventFailure: return "Event failure";
            case Message::TimeoutError: return "Timeout error";
            case Message::IllegalState: return "Illegal state";
            default: return "";
        }
    }

    class BleLogger {
        public:
            BleLogger(TimeLib::Clock& clk, SdFat& sd, HciLib::HciUart& hci, Stream* dbg = nullptr, Util::LogLevel level = Util::LogLevel::Warning);
            void heartBeat();

        private:
            const Util::LogLevel _logLevel;
            const char* _logFilePath;
            const uint8_t _chipSelect;
            const time_t _timeSyncInterval;

            State _previousState;
            State _state;
            Message _stateMessage;

            Stream* _dbg;
            TimeLib::Clock& _clk;
            SdFat& _sd;
            HciLib::HciUart& _hci;

            template <typename T> void msg(const Util::LogLevel level, const T* message, const bool newline = false);
            void twoPartMsg(const Util::LogLevel level, const char subject[], const char message[]);
            void stateMsg(const Util::LogLevel level, const State state, const Message message);
            void transitionMsg(const Util::LogLevel level, const State previous, const State next, const Message message = Message::None);
            void transition(const State state, const Message message = Message::None);

            void uninitialized();
            void serialSetup();
            void clockSync();
            void sdSetup();
            void initialized();
            void dispatchGdi();
            void awaitGdiHecs();
            void awaitGdid();
            void dispatchSem();
            void awaitSemCc();
            void dispatchLsem();
            void awaitLsemCc();
            void dispatchLssp();
            void awaitLsspCc();
            void dispatchLsse();
            void awaitLsseCc();
            void scanning();
            void dispatchReset();
            void awaitRCc();
            void error();
            void critical() {};
            void illegalState();
    };
}
