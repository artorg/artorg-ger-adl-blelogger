#pragma once

#include "blelogger_mock.hpp"

#ifndef ARDUINO
#include <stddef.h>
#include <stdint.h>

#define FILE_WRITE 1

class File : public Print {
    public:
        bool isOpen() { return true; };
        void close() {};
};

class SdFat {
    public:
        bool begin(const uint8_t chipSelect) { return true; };
        File open(const char path[], uint8_t mode) { return File(); };
};

#else
#include "SdFat.h"
#endif
