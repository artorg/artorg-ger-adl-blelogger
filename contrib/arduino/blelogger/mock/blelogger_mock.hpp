#pragma once

#ifndef ARDUINO
#include "stddef.h"
#include "stdint.h"

#define F(x) x

class Print {
    public:
        int getWriteError() { return 0; };
        void clearWriteError() {};
        size_t write(const uint8_t b) { return 0; };
        size_t write(const uint8_t buf[], const size_t bufLen) { return 0; };
        size_t print(const char c[]) { return 0; };
        size_t println(const char c[]) { return 0; };
};

class Stream : public Print {
    public:
        int available() { return 64; };
        int read() { return 0; };
        int peek() { return 0; };
        void flush() {};
        void setTimeout(unsigned long timeout) {};
        bool find(char target[]) { return false; };
        bool findUntil(char target[], char terminator[]) { return false; };
        size_t readBytes(uint8_t buffer[], size_t length) { return 0; };
        size_t readBytesUntil(char terminator, uint8_t buffer[], size_t length) { return 0; };
};

class HardwareSerial : public Stream {
    public:
        void begin(unsigned long) {};
        void end() {};
        operator bool() { return true; }
} Serial, Serial1;

#else
#include "Arduino.h"
#endif
