/*
 * hcilib.cpp
 * Copyright (C) 2016 young <young@staff-201-71.eduroam.unibe.ch>
 *
 * Distributed under terms of the MIT license.
 */

#include "hcilib.hpp"

using namespace HciLib;

HciUart::HciUart(Stream& serial, Stream* dbg, Util::LogLevel level) :
    _logLevel(level),
    _littleEndian(true),
    _awaitTimeout(128),
    _profile(GapProfile::Central),
    _maxScanResponses(5),
    _signCounter(0),
    _scanType(ScanType::PassiveScan),
    _scanInterval(0x0010U),
    _scanWindow(0x0010U),
    _addressType(AddressType::PublicIdentityAddress),
    _filterPolicy(FilterPolicy::UndirectedAdsOnly),
    _scanEnable(true),
    _filterDuplicates(true),
    _serial(serial),
    _dbg(dbg),
    _await()
{}

bool HciUart::gapDeviceInit() {
    CommandPacket packet;
    packet.command = Command::GapDeviceInit;
    packet.paramLen = 2 + 2 * resolvingKeyLen + sizeof(_signCounter);

    // Assign the first two bytes of the parameter buffer
    packet.params[0] = static_cast<uint8_t>(_profile);
    packet.params[1] = _maxScanResponses;

    // Assign the IRK and the CSRK
    for (uint8_t i = 2; i < packet.paramLen - sizeof(_signCounter); i++) {
        packet.params[i] = 0;
        packet.params[i + resolvingKeyLen] = 0;
    }

    // Assign the sign counter
    // FIXME: The sign counter is not assigned correctly yet.
    for (uint8_t i = packet.paramLen - sizeof(_signCounter); i < packet.paramLen; i++) {
        packet.params[i] = 0;
    }

    return sendPacket(packet);
}

bool HciUart::setEventMask() {
    CommandPacket packet;
    packet.command = Command::SetEventMask;
    packet.paramLen = 8;

    const uint8_t LeMetaEventByte = 7;
    for (uint8_t i = 0; i < packet.paramLen; i++) {
        if (i == LeMetaEventByte) {
            packet.params[i] = 0x20U;
        } else {
            packet.params[i] = 0x00U;
        }
    }

    return sendPacket(packet);
}

bool HciUart::leSetEventMask() {
    CommandPacket packet;
    packet.command = Command::LESetEventMask;
    packet.paramLen = 8;

    const uint8_t LeAdvertisingReportByte = 0;
    for (uint8_t i = 0; i < packet.paramLen; i++) {
        if (i == LeAdvertisingReportByte) {
            packet.params[i] = 0x02U;
        } else {
            packet.params[i] = 0x00U;
        }
    }

    return sendPacket(packet);
}

bool HciUart::leSetScanParameters() {
    CommandPacket packet;
    packet.command = Command::LESetScanParameters;
    packet.paramLen = 7;

    // Assign the first parameter byte
    packet.params[0] = static_cast<uint8_t>(_scanType);

    // Assign the scan interval
    Util::fromUint16(_scanInterval, packet.params[1], packet.params[2], _littleEndian);

    // Assign the scan window
    Util::fromUint16(_scanWindow, packet.params[3], packet.params[4], _littleEndian);

    // Assign the last two parameter bytes
    packet.params[5] = static_cast<uint8_t>(_addressType);
    packet.params[6] = static_cast<uint8_t>(_filterPolicy);

    return sendPacket(packet);
};

bool HciUart::leSetScanEnable() {
    CommandPacket packet;
    packet.command = Command::LESetScanEnable;
    packet.paramLen = 2;

    // Assign the two parameters
    packet.params[0] = static_cast<uint8_t>(_scanEnable);
    packet.params[1] = static_cast<uint8_t>(_filterDuplicates);

    return sendPacket(packet);
}

bool HciUart::reset() {
    CommandPacket packet;
    packet.command = Command::Reset;
    packet.paramLen = 0;

    return sendPacket(packet);
}

Await HciUart::awaitCc(const Command command) {
    auto status = awaitPacket(AwaitOwner::CommandComplete, PacketType::Event, static_cast<uint16_t>(Event::CommandComplete));
    if (status == Await::Success) {
        auto numCommands = _await.params[0];
        auto opcode = Util::toUint16(_await.params[1], _await.params[2], _littleEndian);
        auto cmdStatus = _await.params[3];

        msg(Util::LogLevel::Debug, F("CommandStatus parameters: "));
        varMsg(Util::LogLevel::Debug, F("numCommands"), numCommands);
        msg(Util::LogLevel::Debug, F(", "));
        varMsg(Util::LogLevel::Debug, F("opcode"), opcode);
        msg(Util::LogLevel::Debug, F(", "));
        varMsg(Util::LogLevel::Debug, F("cmdStatus"), cmdStatus, true);

        if (opcode == static_cast<uint16_t>(command) && cmdStatus == static_cast<uint8_t>(Status::Success)) {
            return Await::Success;
        } else {
            return Await::Failure;
        }
    } else {
        return status;
    }
}

Await HciUart::awaitLar(SdFat& sd, const char filePath[]) {
    auto status = awaitPacket(AwaitOwner::LEAdvertisingReport,
            PacketType::Event, static_cast<uint16_t>(Event::LE), &sd,
            filePath);
    if (status == Await::Success) {
        auto subevent = _await.params[0];

        msg(Util::LogLevel::Debug, F("LEAdvertisingReport parameters: "));
        varMsg(Util::LogLevel::Debug, F("subevent"), subevent, true);

        if (subevent == static_cast<uint8_t>(LEEvent::LEAdvertisingReport)) {
            return Await::Success;
        } else {
            return Await::Failure;
        }
    } else {
        return status;
    }
}

Await HciUart::awaitGdid() {
    auto status = awaitPacket(AwaitOwner::GapDeviceInitDone, PacketType::Event,
            static_cast<uint16_t>(Event::VendorSpecific));
    if (status == Await::Success) {
        auto vendorEvent = Util::toUint16(_await.params[0], _await.params[1], _littleEndian);
        auto eventStatus = _await.params[2];

        msg(Util::LogLevel::Debug, F("GapDeviceInitDone parameters: "));
        varMsg(Util::LogLevel::Debug, F("eventStatus"), eventStatus);
        msg(Util::LogLevel::Debug, F(", ..."), true);

        if (vendorEvent ==
                static_cast<uint16_t>(VendorEvent::GapDeviceInitDone) &&
                eventStatus == static_cast<uint8_t>(ExtStatus::Success)) {
            return Await::Success;
        } else {
            return Await::Failure;
        }
    } else {
        return status;
    }
}

Await HciUart::awaitHecs(const Command command) {
    auto status = awaitPacket(AwaitOwner::HciExtCommandStatus,
            PacketType::Event, static_cast<uint16_t>(Event::VendorSpecific));
    if (status == Await::Success) {
        auto vendorEvent = Util::toUint16(_await.params[0], _await.params[1], _littleEndian);
        auto eventStatus = _await.params[2];
        auto opcode = Util::toUint16(_await.params[3], _await.params[4], _littleEndian);

        msg(Util::LogLevel::Debug, F("HciExtCommandStatus parameters: "));
        varMsg(Util::LogLevel::Debug, F("vendorEvent"), vendorEvent);
        msg(Util::LogLevel::Debug, F(", "));
        varMsg(Util::LogLevel::Debug, F("eventStatus"), eventStatus);
        msg(Util::LogLevel::Debug, F(", "));
        varMsg(Util::LogLevel::Debug, F("opcode"), opcode, true);

        if (vendorEvent ==
                static_cast<uint16_t>(VendorEvent::HciExtCommandStatus) &&
                eventStatus == static_cast<uint8_t>(ExtStatus::Success) &&
                opcode == static_cast<uint16_t>(command)) {
            return Await::Success;
        } else {
            return Await::Failure;
        }
    } else {
        return status;
    }
}

const char* HciUart::getInternalStateMessage() {
    return getAwaitMessage(_await.message);
}

void HciUart::discardBuffer() {
    msg(Util::LogLevel::Debug, F("Buffer contents discarded ("));
    varMsg(Util::LogLevel::Debug, F("numBytes"), ifaceAvailable());
    msg(Util::LogLevel::Debug, F("): "));
    while (ifaceAvailable() > 0) {
        ifaceRead();
    }
    if (ifaceAvailable() == 0) {
        msg(Util::LogLevel::Debug, "", true);
    }
}

bool HciUart::sendPacket(const CommandPacket& packet) {
    uint8_t pktHeader[commandHeaderLen];
    pktHeader[0] = static_cast<uint8_t>(PacketType::Command);
    Util::fromUint16(static_cast<uint16_t>(packet.command), pktHeader[1], pktHeader[2], _littleEndian);
    pktHeader[commandHeaderLen - 1] = packet.paramLen;

    // Print the packet to debug
    if (_logLevel <= Util::LogLevel::Debug) {
        msg(Util::LogLevel::Debug, F("Send: "));
        for (uint8_t i = 0; i < commandHeaderLen; i++) {
            hexMsg(Util::LogLevel::Debug, pktHeader[i]);
            if (i < commandHeaderLen - 1) {
                msg(Util::LogLevel::Debug, F(":"));
            } else {
                msg(Util::LogLevel::Debug, F("#"));
            }
        }
        for (uint8_t i = 0; i < packet.paramLen; i++) {
            hexMsg(Util::LogLevel::Debug, packet.params[i]);
        }
        msg(Util::LogLevel::Debug, "", true);
    }

    //return s && _serial.write(pktHeader, commandHeaderLen) && _serial.write(packet.params, packet.paramLen);
    _serial.write(pktHeader, commandHeaderLen);
    _serial.write(packet.params, packet.paramLen);
    return true;
}

Await HciUart::awaitPacket(const AwaitOwner owner, const PacketType type, const
        uint16_t identifier, SdFat* sd, const char* logFilePath) {
    // Am I the owner of the stream?
    if (_await.owner == AwaitOwner::Nobody) {
        ownState(owner);
    } else if (_await.owner != owner) {
        return Await::Busy;
    }

    // Is the timeout over?
    if (_await.timeout == 0) {
        resetState();
        return Await::Timeout;
    }

    // Am I reading the stream already?
    if (_await.status != Await::Waiting) {
        // Continue reading the packet body.
        // Can I read the header?
        if (ifaceAvailable() >= maxHeaderLen) {
            if (parseHeader(_await.type, _await.identifier, _await.paramLen)) {
                _await.status = Await::Waiting;
                if (_await.type == type && _await.identifier == identifier && !(sd == nullptr || logFilePath == nullptr)) {
                    auto logFile = sd->open(logFilePath, FILE_WRITE);
                    if (logFile.isOpen()) {
                        saveHeader(logFile, _await.type, _await.identifier, _await.paramLen);
                        logFile.close();
                    } else {
                        _await.message = AwaitMessage::LogError;
                    }
                }
            } else {
                resetState();
                return Await::Failure;
            }
        } else {
            _await.message = AwaitMessage::HeaderIncomplete;
            decTimeout();
            return Await::Waiting;
        }
    }

    // Is the rest of the packet available? Discard the bytes if we're not
    // interested.
    if (_await.type == type && _await.identifier == identifier) {
        savePacket(_await.paramLen, _await.paramIdx, _await.params, maxParamCacheLen, sd, logFilePath);
        // Have we finished reading the packet?
        if (_await.paramIdx == _await.paramLen) {
            resetState();
            return Await::Success;
        } else {
            _await.message = AwaitMessage::BodyIncomplete;
            decTimeout();
            return Await::Waiting;
        }
    } else {
        discardPacket(_await.paramLen, _await.paramIdx);
        // Have we finished reading the packet?
        if (_await.paramIdx == _await.paramLen) {
            resetState();
        }
        _await.message = AwaitMessage::DiscardIncomplete;
        decTimeout();
        return Await::Waiting;
    }
}

bool HciUart::parseHeader(PacketType& type, uint16_t& identifier, uint16_t& paramLen) {
    msg(Util::LogLevel::Debug, F("Receive: "));
    type = static_cast<PacketType>(ifaceRead());
    msg(Util::LogLevel::Debug, F(":"));

    bool success = false;
    switch (type) {
        case PacketType::Command:
            identifier = Util::toUint16(ifaceRead(), ifaceRead(), _littleEndian);
            msg(Util::LogLevel::Debug, F(":"));
            paramLen = ifaceRead();
            success = true;
            break;

        case PacketType::Async:
            identifier = Util::toUint16(ifaceRead(), ifaceRead(), _littleEndian);
            msg(Util::LogLevel::Debug, F(":"));
            paramLen = Util::toUint16(ifaceRead(), ifaceRead(), _littleEndian);
            success = true;
            break;

        case PacketType::Event:
            identifier = ifaceRead();
            msg(Util::LogLevel::Debug, F(":"));
            paramLen = ifaceRead();
            success = true;
            break;

        default:
            break;
    }

    msg(Util::LogLevel::Debug, F("#"));
    return success;
}

void HciUart::saveHeader(Print& file, const PacketType type, const uint16_t identifier, const uint16_t paramLen) {
    file.write(static_cast<uint8_t>(type));
    uint8_t tmpA;
    uint8_t tmpB;

    switch (type) {
        case PacketType::Command:
            Util::fromUint16(identifier, tmpA, tmpB, _littleEndian);
            file.write(tmpA);
            file.write(tmpB);
            file.write(static_cast<uint8_t>(paramLen));
            break;

        case PacketType::Async:
            Util::fromUint16(identifier, tmpA, tmpB, _littleEndian);
            file.write(tmpA);
            file.write(tmpB);
            Util::fromUint16(paramLen, tmpA, tmpB, _littleEndian);
            file.write(tmpA);
            file.write(tmpB);
            break;
        case PacketType::Event:
            file.write(static_cast<uint8_t>(identifier));
            file.write(static_cast<uint8_t>(paramLen));
            break;
        default:
            break;
    }
}

void HciUart::savePacket(const uint16_t paramLen, uint16_t& paramIdx, uint8_t
        params[], const uint8_t pCacheLen, SdFat* sd, const char* logFilePath)
{
    if (!(sd == nullptr || logFilePath == nullptr)) {
        auto logFile = sd->open(logFilePath, FILE_WRITE);
        if (logFile.isOpen()) {
            while (ifaceAvailable() > 0 && paramIdx < paramLen) {
                auto b = ifaceRead();
                logFile.write(b);
                if (0 <= paramIdx && paramIdx < pCacheLen) {
                    params[paramIdx] = b;
                }
                paramIdx++;
            }
            logFile.close();
            if (paramIdx == paramLen) {
                msg(Util::LogLevel::Debug, "", true);
            }
            return;
        }
    }

    while (ifaceAvailable() > 0 && paramIdx < paramLen) {
        auto b = ifaceRead();
        if (0 <= paramIdx && paramIdx < pCacheLen) {
            params[paramIdx] = b;
        }
        paramIdx++;
    }

    if (paramIdx == paramLen) {
        msg(Util::LogLevel::Debug, "", true);
    }
}

void HciUart::discardPacket(const uint16_t paramLen, uint16_t& paramIdx) {
    while (ifaceAvailable() > 0 && paramIdx < paramLen) {
        ifaceRead();
        paramIdx++;
    }

    if (paramIdx == paramLen) {
        msg(Util::LogLevel::Debug, "", true);
    }
}

template <typename T> void HciUart::msg(const Util::LogLevel level, const T* message, const bool newline) {
    if (_dbg != nullptr && level >= _logLevel) {
        if (newline) {
            _dbg->println(message);
        } else {
            _dbg->print(message);
        }
    }
}

template <typename T> void HciUart::varMsg(const Util::LogLevel level, const T* name, const uint8_t number, const bool newline) {
    if (level >= _logLevel) {
        char cBuf[2 * sizeof(number) + 1];
        Util::toHex(&number, sizeof(number), cBuf, sizeof(cBuf));
        msg(level, name);
        msg(level, F("=0x"));
        msg(level, cBuf, newline);
    }
}

template <typename T> void HciUart::varMsg(const Util::LogLevel level, const T* name, const uint16_t number, const bool newline) {
    if (level >= _logLevel) {
        uint8_t bBuf[sizeof(number)];
        char cBuf[2 * sizeof(number) + 1];
        Util::fromUint16(number, bBuf[0], bBuf[1], false);
        Util::toHex(bBuf, sizeof(number), cBuf, sizeof(cBuf));
        msg(level, name);
        msg(level, F("=0x"));
        msg(level, cBuf, newline);
    }
}

void HciUart::hexMsg(const Util::LogLevel level, const uint8_t number, const bool newline) {
    if (level >= _logLevel) {
        char cBuf[2 * sizeof(number) + 1];
        Util::toHex(&number, sizeof(number), cBuf, sizeof(cBuf));
        msg(level, cBuf, newline);
    }
}

void HciUart::hexMsg(const Util::LogLevel level, const uint16_t number, const bool newline) {
    if (level >= _logLevel) {
        uint8_t bBuf[sizeof(number)];
        char cBuf[2 * sizeof(number) + 1];
        Util::fromUint16(number, bBuf[0], bBuf[1], false);
        Util::toHex(bBuf, sizeof(number), cBuf, sizeof(cBuf));
        msg(level, cBuf, newline);
    }
}

uint8_t HciUart::ifaceRead() {
    auto b = static_cast<uint8_t>(_serial.read());
    hexMsg(Util::LogLevel::Debug, b);
    return b;
}

uint8_t HciUart::ifaceAvailable() {
    return static_cast<uint8_t>(_serial.available());
}

void HciUart::resetState() {
    _await.owner = AwaitOwner::Nobody;
    _await.status = Await::Inactive;
    _await.timeout = _awaitTimeout;
    _await.paramIdx = 0;
}

void HciUart::ownState(const AwaitOwner owner) {
    resetState();
    _await.owner = owner;
}

void HciUart::decTimeout() {
    _await.timeout--;
}
