#pragma once

#include <stdint.h>

namespace HciLib {
    enum class Status : uint8_t {
        Success = 0x00U,
        UnknownHciCommand = 0x01U,
        UnknownConnectionIdentifier = 0x02U,
        HardwareFailure = 0x03U,
        PageTimeout = 0x04U,
        AuthenticationFailure = 0x05U,
        PinOrKeyMissing = 0x06U,
        MemoryCapacityExceeded = 0x07U,
        ConnectionTimeout = 0x08U,
        ConnectionLimitExceeded = 0x09U,
        SynchronousConnectionLimitExceeded = 0x0aU,
        ACLConnectionAlreadyExists = 0x0bU,
        CommandDisallowed = 0x0cU,
        ConnectionRejectedLimitedResources = 0x0dU,
        ConnectionRejectedSecurityReasons = 0x0eU,
        ConnectionRejectedUnnacceptableBDAddr = 0x0fU,
        ConnectionAcceptTimeoutExceeded = 0x10U,
        UnsupportedFeatureOrParameterValue = 0x11U,
        InvalidHciCommandParameters = 0x12U,
        RemoteUserTerminatedConnection = 0x13U,
        RemoteDeviceTerminatedConnectionLowResources = 0x14U,
        RemoteDeviceTerminatedConnectionPowerOff = 0x15U,
        ConnectionTerminatedLocalHost = 0x16U,
        RepeatedAttempts = 0x17U,
        PairingNotAllowed = 0x18U,
        UnknownLmpPdu = 0x19U,
        UnsupportedRemoteFeature = 0x1aU,
        ScoOffsetRejected = 0x1bU,
        ScoIntervalRejected = 0x1cU,
        ScoAirModeRejected = 0x1dU,
        InvalidLmpParameters = 0x1eU,
        UnspecifiedError = 0x1fU,
        UnsupportedLmpParameterValue = 0x20U,
        RoleChangeNotAllowed = 0x21U,
        LmpResponseTimeout = 0x22U,
        LmpErrorTransactionCollision = 0x23U,
        LmpPduNotAllowed = 0x24U,
        EncryptionModeNotAcceptable = 0x25U,
        LinkKeyCannotChange = 0x26U,
        RequestedQosNotSupported = 0x27U,
        InstantPassed = 0x28U,
        PairingWithUnitKeyNotSupported = 0x29U,
        DifferentTransactionCollision = 0x2aU,
        QosUnnacceptableParameter = 0x2cU,
        QosRejected = 0x2dU,
        ChannelClassificationNotSupported = 0x2eU,
        InsufficientSecurity = 0x2fU,
        ParameterOutOfMandatoryRange = 0x30U,
        RoleSwitchPending = 0x32U,
        RoleSwitchFailed = 0x35U,
        ExtendedInquiryResponseTooLarge = 0x36U,
        SecureSimplePairingNotSupportedByHost = 0x37U,
        HostBusyPairing = 0x38U,
        ConnectionRejectedNoSuitableChannel = 0x39U,
        ControllerBusy = 0x3aU,
        UnacceptableConnectionParameters = 0x3bU,
        DirectedAdvertisingTimeout = 0x3cU,
        ConnectionTerminatedMicFailure = 0x3dU,
        ConnectionEstablishFailure = 0x3eU,
        MacConnectionFailed = 0x3fU,
        CoarseClockAdjustmentRejected = 0x40U
    };

    enum class ExtStatus : uint8_t {
        Success = 0x00U,
        Failure = 0x01U,
        InvalidParameter = 0x02U,
        InvalidTask = 0x03U,
        MessageBufferNotAvailable = 0x04U,
        InvalidMessagePointer = 0x05U,
        InvalidEventID = 0x06U,
        InvalidInterruptID = 0x07U,
        NoTimerAvailable = 0x08U,
        NonVolatileItemUninitialized = 0x09U,
        NonVolatileOperationFailed = 0x0aU,
        InvalidMemorySize = 0x0bU,
        NonVolatileBadItemLength = 0x0cU,
        BleNotReady = 0x10U,
        BleAlreadyInRequestedMode = 0x11U,
        BleIncorrectMode = 0x12U,
        BleMemoryAllocation = 0x13U,
        BleNotConnected = 0x14U,
        BleNoResources = 0x15U,
        BlePending = 0x16U,
        BleTimeout = 0x17U,
        BleInvalidRange = 0x18U,
        BleLinkEncrypted = 0x19U,
        BleProcedureComplete = 0x1aU,
        BleGapUserCanceled = 0x30U,
        BleGapConnectionNotAcceptable = 0x31U,
        BleGapBondRejected = 0x32U,
        BleInvalidPdu = 0x40U,
        BleInsufficientAuthentication = 0x41U,
        BleInsufficientEncryption = 0x42U,
        BleInsufficientKeySize = 0x43U,
        InvalidTaskID = 0xffU
    };

    enum class Command : uint16_t {
        SetEventMask = 0x0c01U,
        Reset = 0x0c03U,
        LESetEventMask = 0x2001U,
        LESetScanParameters = 0x200bU,
        LESetScanEnable = 0x200cU,
        GapDeviceInit = 0xfe00U
    };

    enum class Event : uint8_t {
        CommandComplete = 0x0eU,
        CommandStatus = 0x0fU,
        HardwareError = 0x10U,
        DataBufferOverflow = 0x1aU,
        LE = 0x3eU,
        VendorSpecific = 0xffU
    };

    enum class LEEvent : uint8_t {
        LEAdvertisingReport = 0x02U,
    };

    enum class VendorEvent : uint16_t {
        GapDeviceInitDone = 0x0600U,
        HciExtCommandStatus = 0x067fU
    };

    enum class PacketType : uint8_t {
        Invalid = 0x00U,
        Command = 0x01U,
        Async = 0x02U,
        Sync = 0x03U,
        Event = 0x04U
    };

    enum class GapProfile : uint8_t {
        Broadcaster = 0x01U,
        Observer = 0x02U,
        Peripheral = 0x04U,
        Central = 0x08U
    };

    enum class DiscoveryType : uint8_t {
        ConnectableUndirectedAdvertising = 0x00U,
        ConnectableDirectedAdvertising = 0x01U,
        ScannableUndirectedAdvertising = 0x02U,
        NonConnectableUndirectedAdvertising = 0x03U,
        ScanResponse = 0x04U
    };

    enum class AddressType : uint8_t {
        PublicDeviceAddress = 0x00U,
        RandomDeviceAddress = 0x01U,
        PublicIdentityAddress = 0x02U,
        RandomIdentityAddress = 0x03U
    };

    enum class ScanType : uint8_t {
        PassiveScan = 0x00U,
        ActiveScan = 0x01U
    };

    enum class FilterPolicy : uint8_t {
        UndirectedAdsOnly = 0x00U,
        WhitelistedOnly = 0x01U,
        ResolvableDirected = 0x02U,
        WhitelistedAndResolvableDirected = 0x03U
    };

    enum class ResetMode : uint8_t {
        Hard = 0x00U,
        Soft = 0x01U
    };
}
