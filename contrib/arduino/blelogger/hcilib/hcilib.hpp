#pragma once

#include "hciapi.hpp"
#include "../util.hpp"
#include "../mock/blelogger_mock.hpp"
#include "../mock/sdfat_mock.hpp"

#include <stdint.h>

namespace HciLib {
    const uint32_t hciBaudrate = 115200;
    const uint8_t maxPacketLen = 128;
    const uint8_t maxParamLen = 125;
    const uint8_t maxParamCacheLen = 8;
    const uint8_t maxDataLen = 31;
    const uint8_t maxNumAdvReports = 0x19U;
    const uint8_t commandHeaderLen = 4;
    const uint8_t asyncHeaderLen = 5;
    const uint8_t eventHeaderLen = 3;
    const uint8_t maxHeaderLen = asyncHeaderLen;
    const uint8_t minHeaderLen = eventHeaderLen;
    const uint8_t resolvingKeyLen = 16;
    const uint8_t bleAddressLen = 6;

    enum class AwaitOwner : uint8_t {
        Nobody,
        CommandComplete,
        LEAdvertisingReport,
        GapDeviceInitDone,
        HciExtCommandStatus
    };

    enum class Await : uint8_t {
        Inactive,
        Busy,
        Success,
        Waiting,
        Timeout,
        Failure
    };

    enum class AwaitMessage : uint8_t {
        None,
        HeaderIncomplete,
        BodyIncomplete,
        DiscardIncomplete,
        BufferDiscarded,
        LogError
    };

    struct AwaitState {
        AwaitOwner owner;
        Await status;
        AwaitMessage message;
        uint16_t timeout;
        PacketType type;
        uint16_t identifier;
        uint16_t paramLen;
        uint16_t paramIdx;
        uint8_t params[maxParamCacheLen];
    };

    struct CommandPacket {
        Command command;
        uint8_t paramLen;
        uint8_t params[maxParamLen];
    };

    struct LEAdvertisingReport {
        uint8_t numReports;
        DiscoveryType discoveryType[maxNumAdvReports];
        AddressType addressType[maxNumAdvReports];
        uint8_t addresses[bleAddressLen * maxNumAdvReports];
        uint8_t dataLen[maxNumAdvReports];
        uint8_t dataFields[maxDataLen * maxNumAdvReports];
        int8_t rssi[maxNumAdvReports];
    };

    static const char* getAwaitMessage(AwaitMessage m) {
        switch (m) {
            case AwaitMessage::None: return "None";
            case AwaitMessage::HeaderIncomplete: return "Header incomplete";
            case AwaitMessage::BodyIncomplete: return "Body incomplete";
            case AwaitMessage::DiscardIncomplete: return "Discard incomplete";
            case AwaitMessage::BufferDiscarded: return "Buffer discarded";
            case AwaitMessage::LogError: return "Log write error";
            default: return "";
        }
    }

    class HciUart {
        public:
            HciUart(Stream& serial, Stream* dbg = nullptr,
                    Util::LogLevel level = Util::LogLevel::Warning);

            bool gapDeviceInit();
            bool setEventMask();
            bool leSetEventMask();
            bool leSetScanParameters();
            bool leSetScanEnable();
            bool reset();

            Await awaitCc(const Command command);
            Await awaitLar(SdFat& sd, const char filePath[]);
            Await awaitGdid();
            Await awaitHecs(const Command command);

            const char* getInternalStateMessage();
            void discardBuffer();

        private:
            const Util::LogLevel _logLevel;
            const bool _littleEndian;
            const uint16_t _awaitTimeout;
            const GapProfile _profile;
            const uint8_t _maxScanResponses;
            const uint32_t _signCounter;
            const ScanType _scanType;
            const uint16_t _scanInterval;
            const uint16_t _scanWindow;
            const AddressType _addressType;
            const FilterPolicy _filterPolicy;
            const bool _scanEnable;
            const bool _filterDuplicates;

            Stream& _serial;
            Stream* _dbg;
            AwaitState _await;

            template <typename T> void msg(const Util::LogLevel level, const T* message, const bool newline = false);
            template <typename T> void varMsg(const Util::LogLevel level, const T* name, const uint8_t number, const bool newline = false);
            template <typename T> void varMsg(const Util::LogLevel level, const T* name, const uint16_t number, const bool newline = false);
            void hexMsg(const Util::LogLevel level, const uint8_t number, const bool newline = false);
            void hexMsg(const Util::LogLevel level, const uint16_t number, const bool newline = false);
            uint8_t ifaceRead();
            uint8_t ifaceAvailable();

            bool sendPacket(const CommandPacket& packet);
            Await awaitPacket(
                    const AwaitOwner owner,
                    const PacketType type,
                    const uint16_t identifier,
                    SdFat* sd = nullptr,
                    const char* logFilePath = nullptr
                    );
            bool parseHeader(
                    PacketType& type,
                    uint16_t& identifier,
                    uint16_t& paramLen
                    );
            void saveHeader(
                    Print& file,
                    const PacketType type,
                    const uint16_t identifier,
                    const uint16_t paramLen
                    );
            void savePacket(
                    const uint16_t paramLen,
                    uint16_t& paramIdx,
                    uint8_t params[],
                    const uint8_t pCacheLen,
                    SdFat* sd = nullptr,
                    const char* logFilePath = nullptr
                    );
            void discardPacket(const uint16_t paramLen, uint16_t& paramIdx);

            void resetState();
            void ownState(const AwaitOwner owner);
            void decTimeout();
    };
}
