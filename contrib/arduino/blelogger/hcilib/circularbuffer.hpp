#pragma once

#include <stddef.h>
#include <stdint.h>

namespace Container {
    template <typename T, size_t bufLen>
        class CircularBuffer {
            public:
                CircularBuffer() : _writeIdx(0), _readIdx(0), _size(0) {};
                ~CircularBuffer() {};

                // Container-like interface
                void pop_front() {
                    _readIdx = (_readIdx + 1) % bufLen;
                    _size--;
                };

                void push_back(const T value) {
                    _buffer[_writeIdx] = value;
                    _writeIdx = (_writeIdx + 1) % bufLen;
                    _size++;
                };

                bool empty() const {
                    return _size == 0;
                };

                size_t size() const {
                    return _size;
                };

                T at(size_t n) {
                    size_t idx = (_readIdx + n) % bufLen;
                    return _buffer[idx];
                };

                T front() {
                    return at(0);
                };

                void erase(size_t n) {
                    for (size_t i = 0; i < n; i++) {
                        pop_front();
                    }
                };

                // Stream-like interface
                T read() {
                    T result = front();
                    pop_front();
                    return result;
                }

                size_t write(const T value) {
                    push_back(value);
                    return 1;
                }

                size_t available() const {
                    return size();
                }

            private:
                T _buffer[bufLen];
                size_t _writeIdx;
                size_t _readIdx;
                size_t _size;
        };
}
