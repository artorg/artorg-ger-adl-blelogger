/*
 * time.cpp
 * Copyright (C) 2016 young <young@staff-201-2.eduroam.unibe.ch>
 *
 * Distributed under terms of the MIT license.
 */

#include "timelib.hpp"

using namespace TimeLib;

Clock::Clock() :
    _cache(),
    _time(0),
    _syncInterval(300),
    _sysTime(0),
    _prevMillis(0),
    _nextSyncTime(0),
    _status(TimeStatus::NotSet),
#ifdef TIME_DRIFT_INFO
    _sysUnsyncedTime(0),
#endif
    _getTimePtr(nullptr)
{}

void Clock::refreshCache(time_t t) {
    if (t != _time) {
        breakTime(t, _cache);
        _time = t;
    }
}

int Clock::hour() { // the hour now
    return hour(now());
}

int Clock::hour(time_t t) { // the hour for the given time
    refreshCache(t);
    return _cache.Hour;
}

int Clock::hourFormat12() { // the hour now in 12 hour format
    return hourFormat12(now());
}

int Clock::hourFormat12(time_t t) { // the hour for the given time in 12 hour format
    refreshCache(t);
    if( _cache.Hour == 0 )
        return 12; // 12 midnight
    else if( _cache.Hour  > 12)
        return _cache.Hour - 12 ;
    else
        return _cache.Hour ;
}

bool Clock::isAM() { // returns true if time now is AM
    return !isPM(now());
}

bool Clock::isAM(time_t t) { // returns true if given time is AM
    return !isPM(t);
}

bool Clock::isPM() { // returns true if PM
    return isPM(now());
}

bool Clock::isPM(time_t t) { // returns true if PM
    return (hour(t) >= 12);
}

int Clock::minute() {
    return minute(now());
}

int Clock::minute(time_t t) { // the minute for the given time
    refreshCache(t);
    return _cache.Minute;
}

int Clock::second() {
    return second(now());
}

int Clock::second(time_t t) {  // the second for the given time
    refreshCache(t);
    return _cache.Second;
}

int Clock::day() {
    return(day(now()));
}

int Clock::day(time_t t) { // the day for the given time (0-6)
    refreshCache(t);
    return _cache.Day;
}

int Clock::weekday() {   // Sunday is day 1
    return  weekday(now());
}

int Clock::weekday(time_t t) {
    refreshCache(t);
    return _cache.Wday;
}

int Clock::month(){
    return month(now());
}

int Clock::month(time_t t) {  // the month for the given time
    refreshCache(t);
    return _cache.Month;
}

int Clock::year() {  // as in Processing, the full four digit year: (2009, 2010 etc)
    return year(now());
}

int Clock::year(time_t t) { // the year for the given time
    refreshCache(t);
    return _cache.Year + 1970;
}

/*============================================================================*/
/* functions to convert to and from system time */
/* These are for interfacing with time serivces and are not normally needed in a sketch */

// leap year calulator expects year argument as years offset from 1970
#define LEAP_YEAR(Y)     ( ((1970+Y)>0) && !((1970+Y)%4) && ( ((1970+Y)%100) || !((1970+Y)%400) ) )

static const uint8_t monthDays[]={31,28,31,30,31,30,31,31,30,31,30,31}; // API starts months from 1, this array starts from 0

void Clock::breakTime(time_t timeInput, TimeElements& tm){
    // break the given time_t into time components
    // this is a more compact version of the C library localtime function
    // note that year is offset from 1970 !!!

    uint8_t year;
    uint8_t month;
    uint8_t monthLength;
    uint32_t time;
    unsigned long days;

    time = static_cast<uint32_t>(timeInput);
    tm.Second = time % 60;
    time /= 60; // now it is minutes
    tm.Minute = time % 60;
    time /= 60; // now it is hours
    tm.Hour = time % 24;
    time /= 24; // now it is days
    tm.Wday = ((time + 4) % 7) + 1;  // Sunday is day 1

    year = 0;
    days = 0;
    while(static_cast<unsigned>(days += (LEAP_YEAR(year) ? 366 : 365)) <= time) {
        year++;
    }
    tm.Year = year; // year is offset from 1970

    days -= LEAP_YEAR(year) ? 366 : 365;
    time  -= days; // now it is days in this year, starting at 0

    days=0;
    month=0;
    monthLength=0;
    for (month=0; month<12; month++) {
        if (month==1) { // february
            if (LEAP_YEAR(year)) {
                monthLength=29;
            } else {
                monthLength=28;
            }
        } else {
            monthLength = monthDays[month];
        }

        if (time >= monthLength) {
            time -= monthLength;
        } else {
            break;
        }
    }
    tm.Month = month + 1;  // jan is month 1
    tm.Day = static_cast<uint8_t>(time + 1);     // day of month
}

time_t Clock::makeTime(TimeElements& tm) {
    // assemble time elements into time_t
    // note year argument is offset from 1970 (see macros in time.h to convert to other formats)
    // previous version used full four digit year (or digits since 2000),i.e. 2009 was 2009 or 9

    int i;
    time_t seconds;

    // seconds from 1970 till 1 jan 00:00:00 of the given year
    seconds = tm.Year * (_secsPerDay * 365);
    for (i = 0; i < tm.Year; i++) {
        if (LEAP_YEAR(i)) {
            seconds +=  _secsPerDay;   // add extra days for leap years
        }
    }

    // add days for this year, months start from 1
    for (i = 1; i < tm.Month; i++) {
        if ( (i == 2) && LEAP_YEAR(tm.Year)) {
            seconds += _secsPerDay * 29;
        } else {
            seconds += _secsPerDay * monthDays[i-1];  //monthDay array starts from 0
        }
    }
    seconds+= (tm.Day-1) * _secsPerDay;
    seconds+= tm.Hour * _secsPerHour;
    seconds+= tm.Minute * _secsPerMin;
    seconds+= tm.Second;

    return seconds;
}

time_t Clock::now() {
    // calculate number of seconds passed since last call to now()
    while (millis() - _prevMillis >= 1000) {
        // millis() and prevMillis are both unsigned ints thus the subtraction will always be the absolute value of the difference
        _sysTime++;
        _prevMillis += 1000;
#ifdef TIME_DRIFT_INFO
        _sysUnsyncedTime++; // this can be compared to the synced time to measure long term drift
#endif
    }
    if (_nextSyncTime <= _sysTime) {
        if (_getTimePtr != nullptr) {
            time_t t = _getTimePtr();
            if (t != 0) {
                setTime(t);
            } else {
                _nextSyncTime = _sysTime + _syncInterval;
                _status = (_status == TimeStatus::NotSet) ?  TimeStatus::NotSet : TimeStatus::NeedsSync;
            }
        }
    }
    return static_cast<time_t>(_sysTime);
}

void Clock::setTime(time_t t) {
#ifdef TIME_DRIFT_INFO
    if(_sysUnsyncedTime == 0) {
        _sysUnsyncedTime = t;   // store the time of the first call to set a valid Time
    }
#endif

    _sysTime = static_cast<uint32_t>(t);
    _nextSyncTime = static_cast<uint32_t>(t) + _syncInterval;
    _status = TimeStatus::Set;
    _prevMillis = millis();  // restart counting from now (thanks to Korman for this fix)
}

void Clock::setTime(int hr, int min, int sec, int dy, int mnth, int yr){
    // year can be given as full four digit year or two digts (2010 or 10 for 2010);
    //it is converted to years since 1970
    if( yr > 99)
        yr = yr - 1970;
    else
        yr += 30;
    _cache.Year = static_cast<uint8_t>(yr);
    _cache.Month = static_cast<uint8_t>(mnth);
    _cache.Day = static_cast<uint8_t>(dy);
    _cache.Hour = static_cast<uint8_t>(hr);
    _cache.Minute = static_cast<uint8_t>(min);
    _cache.Second = static_cast<uint8_t>(sec);
    setTime(makeTime(_cache));
}

void Clock::adjustTime(long adjustment) {
    _sysTime += adjustment;
}

// indicates if time has been set and recently synchronized
TimeStatus Clock::timeStatus() {
    now(); // required to actually update the status
    return _status;
}

void Clock::setSyncProvider( getExternalTime getTimeFunction){
    _getTimePtr = getTimeFunction;
    _nextSyncTime = _sysTime;
    now(); // this will sync the clock
}

void Clock::setSyncInterval(time_t interval){ // set the number of seconds between re-sync
    _syncInterval = static_cast<uint32_t>(interval);
    _nextSyncTime = _sysTime + _syncInterval;
}
