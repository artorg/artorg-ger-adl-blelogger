#pragma once

#include <stdint.h>

#ifdef ARDUINO
#include <Arduino.h>
typedef uint32_t time_t;
#else
#include <time.h>
static uint32_t millis() { return static_cast<uint32_t>(clock()); };
#endif

namespace TimeLib {
    typedef time_t (*getExternalTime)();

    enum class TimeStatus {
        NotSet,
        NeedsSync,
        Set
    };

    enum class DayOfWeek {
        Invalid,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday
    };

    enum class ByteFields {
        Second,
        Minute,
        Hour,
        Wday,
        Day,
        Month,
        Year,
        NbrFields
    };

    struct TimeElements {
        uint8_t Second;
        uint8_t Minute;
        uint8_t Hour;
        uint8_t Wday;
        uint8_t Day;
        uint8_t Month;
        uint8_t Year;
    };

    class Clock {
        public:
            explicit Clock();

            int hour();            // the hour now
            int hour(time_t t);    // the hour for the given time
            int hourFormat12();    // the hour now in 12 hour format
            int hourFormat12(time_t t); // the hour for the given time in 12 hour format
            bool isAM();            // returns true if time now is AM
            bool isAM(time_t t);    // returns true the given time is AM
            bool isPM();            // returns true if time now is PM
            bool isPM(time_t t);    // returns true the given time is PM
            int minute();          // the minute now
            int minute(time_t t);  // the minute for the given time
            int second();          // the second now
            int second(time_t t);  // the second for the given time
            int day();             // the day now
            int day(time_t t);     // the day for the given time
            int weekday();         // the weekday now (Sunday is day 1)
            int weekday(time_t t); // the weekday for the given time
            int month();           // the month now  (Jan is month 1)
            int month(time_t t);   // the month for the given time
            int year();            // the full four digit year: (2009, 2010 etc)
            int year(time_t t);    // the year for the given time

            time_t now();              // return the current time as seconds since Jan 1 1970
            void setTime(time_t t);
            void setTime(int hr,int min,int sec,int day, int month, int yr);
            void adjustTime(long adjustment);

            /* time sync functions	*/
            TimeStatus timeStatus(); // indicates if time has been set and recently synchronized
            void setSyncProvider(getExternalTime getTimeFunction); // identify the external time provider
            void setSyncInterval(time_t interval); // set the number of seconds between re-sync

        private:
            void refreshCache(time_t t);
            void breakTime(time_t time, TimeElements& tm);  // break time_t into elements
            time_t makeTime(TimeElements& tm);  // convert time elements into time_t

            TimeElements _cache;
            time_t _time;
            uint32_t _syncInterval;
            uint32_t _sysTime;
            uint32_t _prevMillis;
            uint32_t _nextSyncTime;
            TimeStatus _status;
            getExternalTime _getTimePtr;
#ifdef TIME_DRIFT_INFO
            time_t _sysUnsyncedTime;
#endif
            static const time_t _secsPerMin = 60UL;
            static const time_t _secsPerHour = 3600UL;
            static const time_t _secsPerDay = _secsPerHour * 24UL;
            static const time_t _daysPerWeek = 7UL;
            static const time_t _secsPerWeek = _secsPerDay * _daysPerWeek;
            static const time_t _secsPerYear = _secsPerWeek * 52UL;
            static const time_t _secsYr2000 = 946684800UL;
    };
}
