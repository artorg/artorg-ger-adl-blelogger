License
=======

Copyright 2015, Michael Margolis and Paul Stoffregen

Adapted to C++11 by Eleanore Young from the original repository at
https://github.com/PaulStoffregen/Time
