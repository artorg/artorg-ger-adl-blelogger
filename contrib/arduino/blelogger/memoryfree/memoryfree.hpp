// MemoryFree library based on code posted here:
// http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1213583720/15
// Extended by Matthew Murdoch to include walking of the free list.
#pragma once

#if defined(__arm__)
extern "C" char* sbrk(int incr);
#elif defined(__AVR__)
extern char* __brkval;
#endif

namespace MemoryFree{
    static int freeMemory() {
        char top;
#if defined(__arm__)
        return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (defined(ARDUINO) && ARDUINO > 103 && ARDUINO != 151)
        return &top - __brkval;
#elif defined(__AVR__)
        return __brkval ? (&top - __brkval) : (&top - __malloc_heap_start);
#else
        return 0;
#endif
    }
}
