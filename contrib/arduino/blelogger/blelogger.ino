#include "util.hpp"
#include "logger.hpp"

#include "memoryfree/memoryfree.hpp"
#include "timelib/timelib.hpp"
#include "hcilib/hcilib.hpp"

#include "mock/blelogger_mock.hpp"
#include "mock/sdfat_mock.hpp"

#define DEBUG

namespace Sketch {
    // Define the clock, SD-card and logging instances
    const auto debugBaudrate = 9600;
    const auto logLevel = Util::LogLevel::Debug;
    const auto powerLedPin = 13;
    auto clk = TimeLib::Clock();
    auto sd = SdFat();
#ifdef DEBUG
    auto hci = HciLib::HciUart(Serial1, &Serial, Util::LogLevel::Debug);
    auto logger = Logging::BleLogger(clk, sd, hci, &Serial, Util::LogLevel::Debug);
#else
    auto hci = HciLib::HciUart(Serial1);
    auto logger = Logging::BleLogger(clk, sd, hci);
#endif
}

void setup() {
#ifdef DEBUG
        Serial.begin(Sketch::debugBaudrate);
        while (!Serial);
#endif
    Serial1.begin(HciLib::hciBaudrate);

    pinMode(Sketch::powerLedPin, OUTPUT);
    digitalWrite(Sketch::powerLedPin, HIGH);
}

void loop() {
    Sketch::logger.heartBeat();
}
