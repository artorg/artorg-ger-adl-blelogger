/*
 * logger.cpp
 * Copyright (C) 2016 young <young@staff-204-63.eduroam.unibe.ch>
 *
 * Distributed under terms of the MIT license.
 */

#include "logger.hpp"

using namespace Logging;

BleLogger::BleLogger(TimeLib::Clock& clk, SdFat& sd, HciLib::HciUart& hci, Stream* dbg, Util::LogLevel level) :
    _logLevel(level),
    _logFilePath("data.bin"),
    _chipSelect(4),
    _timeSyncInterval(300),
    _previousState(State::Uninitialized),
    _state(State::Uninitialized),
    _stateMessage(Message::None),
    _dbg(dbg),
    _clk(clk),
    _sd(sd),
    _hci(hci)
{}

void BleLogger::heartBeat() {
    switch(_state) {
        case State::Uninitialized: uninitialized(); break;
        case State::SerialSetup: serialSetup(); break;
        case State::ClockSync: clockSync(); break;
        case State::SDSetup: sdSetup(); break;
        case State::Initialized: initialized(); break;
        case State::DispatchGdi: dispatchGdi(); break;
        case State::AwaitGdiHecs: awaitGdiHecs(); break;
        case State::AwaitGdid: awaitGdid(); break;
        case State::DispatchSem: dispatchSem(); break;
        case State::AwaitSemCc: awaitSemCc(); break;
        case State::DispatchLsem: dispatchLsem(); break;
        case State::AwaitLsemCc: awaitLsemCc(); break;
        case State::DispatchLssp: dispatchLssp(); break;
        case State::AwaitLsspCc: awaitLsspCc(); break;
        case State::DispatchLsse: dispatchLsse(); break;
        case State::AwaitLsseCc: awaitLsseCc(); break;
        case State::Scanning: scanning(); break;
        case State::DispatchReset: dispatchReset(); break;
        case State::AwaitRCc: awaitRCc(); break;
        case State::Error: error(); break;
        case State::Critical: critical(); break;
        default: illegalState(); break;
    }
}

void BleLogger::uninitialized() {
    transition(State::SerialSetup);
}

void BleLogger::serialSetup() {
    transition(State::ClockSync);
}

void BleLogger::clockSync() {
    // FIXME: Create the time synchronization provider
    _clk.setSyncProvider(nullptr);
    _clk.setSyncInterval(_timeSyncInterval);
    transition(State::SDSetup);
}

void BleLogger::sdSetup() {
    if (_sd.begin(_chipSelect)) {
        transition(State::Initialized);
    } else {
        transition(State::Critical, Message::SdCardNotAvailable);
    }
}

void BleLogger::initialized() {
    transition(State::DispatchGdi);
}

void BleLogger::dispatchGdi() {
    if (_hci.gapDeviceInit()) {
        transition(State::AwaitGdiHecs);
    } else {
        transition(State::Critical, Message::CommandNotSendable);
    }
}

void BleLogger::awaitGdiHecs() {
    HciLib::Await status = _hci.awaitHecs(HciLib::Command::GapDeviceInit);
    if (status != HciLib::Await::Waiting) {
        if (status == HciLib::Await::Success) {
            transition(State::AwaitGdid);
        } else if (status == HciLib::Await::Timeout) {
            transition(State::Critical, Message::TimeoutError);
        } else {
            transition(State::Critical, Message::CommandFailure);
        }
    }
}

void BleLogger::awaitGdid() {
    HciLib::Await status = _hci.awaitGdid();
    if (status != HciLib::Await::Waiting) {
        if (status == HciLib::Await::Success) {
            transition(State::DispatchSem);
        } else if (status == HciLib::Await::Timeout) {
            transition(State::Critical, Message::TimeoutError);
        } else {
            transition(State::Critical, Message::EventFailure);
        }
    }
}

void BleLogger::dispatchSem() {
    if (_hci.setEventMask()) {
        transition(State::AwaitSemCc);
    } else {
        transition(State::Critical, Message::CommandNotSendable);
    }
}

void BleLogger::awaitSemCc() {
    HciLib::Await status = _hci.awaitCc(HciLib::Command::SetEventMask);
    if (status != HciLib::Await::Waiting) {
        if (status == HciLib::Await::Success) {
            transition(State::DispatchLsem);
        } else if (status == HciLib::Await::Timeout) {
            transition(State::Critical, Message::TimeoutError);
        } else {
            transition(State::Critical, Message::EventFailure);
        }
    }
}

void BleLogger::dispatchLsem() {
    if (_hci.leSetEventMask()) {
        transition(State::AwaitLsemCc);
    } else {
        transition(State::Critical, Message::CommandNotSendable);
    }
}

void BleLogger::awaitLsemCc() {
    HciLib::Await status = _hci.awaitCc(HciLib::Command::LESetEventMask);
    if (status != HciLib::Await::Waiting) {
        if (status == HciLib::Await::Success) {
            transition(State::DispatchLssp);
        } else if (status == HciLib::Await::Timeout) {
            transition(State::Critical, Message::TimeoutError);
        } else {
            transition(State::Critical, Message::EventFailure);
        }
    }
}

void BleLogger::dispatchLssp() {
    if (_hci.leSetScanParameters()) {
        transition(State::AwaitLsspCc);
    } else {
        transition(State::Critical, Message::CommandNotSendable);
    }
}

void BleLogger::awaitLsspCc() {
    HciLib::Await status = _hci.awaitCc(HciLib::Command::LESetScanParameters);
    if (status != HciLib::Await::Waiting) {
        if (status == HciLib::Await::Success) {
            transition(State::DispatchLsse);
        } else if (status == HciLib::Await::Timeout) {
            transition(State::Critical, Message::TimeoutError);
        } else {
            transition(State::Critical, Message::CommandFailure);
        }
    }
}

void BleLogger::dispatchLsse() {
    if (_hci.leSetScanEnable()) {
        transition(State::AwaitLsseCc);
    } else {
        transition(State::Critical, Message::CommandNotSendable);
    }
}

void BleLogger::awaitLsseCc() {
    HciLib::Await status = _hci.awaitCc(HciLib::Command::LESetScanEnable);
    if (status != HciLib::Await::Waiting) {
        if (status == HciLib::Await::Success) {
            transition(State::Scanning);
        } else if (status == HciLib::Await::Timeout) {
            transition(State::Critical, Message::TimeoutError);
        } else {
            transition(State::Critical, Message::CommandFailure);
        }
    }
}

void BleLogger::scanning() {
    HciLib::Await status = _hci.awaitLar(_sd, _logFilePath);
    if (status != HciLib::Await::Waiting) {
        if (status == HciLib::Await::Success) {
            msg(Util::LogLevel::Info, "Wrote log entry.", true);
        } else if (status == HciLib::Await::Timeout) {
            transition(State::Critical, Message::TimeoutError);
        } else {
            transition(State::Critical, Message::EventFailure);
        }
    }
}

void BleLogger::error() {
    transition(State::DispatchReset);
}

void BleLogger::illegalState() {
    transition(State::Critical, Message::IllegalState);
}

void BleLogger::dispatchReset() {
    if (_hci.reset()) {
        transition(State::AwaitRCc);
    } else {
        transition(State::Critical, Message::CommandNotSendable);
    }
}

void BleLogger::awaitRCc() {
    HciLib::Await status = _hci.awaitCc(HciLib::Command::Reset);
    if (status != HciLib::Await::Waiting) {
        if (status == HciLib::Await::Success) {
            transition(State::Initialized);
        } else if (status == HciLib::Await::Timeout) {
            transition(State::Critical, Message::TimeoutError);
        } else {
            transition(State::Critical, Message::CommandFailure);
        }
    }
}

template <typename T> void BleLogger::msg(const Util::LogLevel level, const T* message, const bool newline) {
    if (_dbg != nullptr && level >= _logLevel) {
        if (newline) {
            _dbg->println(message);
        } else {
            _dbg->print(message);
        }
    }
}

void BleLogger::twoPartMsg(const Util::LogLevel level, const char subject[], const char message[]) {
    if (level >= _logLevel) {
        msg(level, subject);
        msg(level, F(": "));
        msg(level, message, true);
    }
}

void BleLogger::stateMsg(const Util::LogLevel level, const State state, const Message message) {
    if (level >= _logLevel) {
        twoPartMsg(level, getStateStr(state), getSateMessage(message));
    }
}

void BleLogger::transitionMsg(const Util::LogLevel level, const State previous, const State next, const Message message) {
    if (level >= _logLevel) {
        msg(level, getStateStr(previous));
        msg(level, F(" -> "));
        msg(level, getStateStr(next));
        if (message != Message::None) {
            msg(level, F(": "));
            msg(level, getSateMessage(message));
            msg(level, F(" (Last HCI message: "));
            msg(level, _hci.getInternalStateMessage());
            msg(level, F(")"));
        }
        msg(level, "", true);
    }
}

void BleLogger::transition(const State state, const Message message) {
    if (state != _state) {
        _previousState = _state;
        _state = state;
        _stateMessage = message;
        if (state == State::Error) {
            transitionMsg(Util::LogLevel::Error, _previousState, _state, _stateMessage);
            _hci.discardBuffer();
        } else if (state == State::Critical) {
            transitionMsg(Util::LogLevel::Critical, _previousState, _state, _stateMessage);
            _hci.discardBuffer();
        } else {
            transitionMsg(Util::LogLevel::Info, _previousState, _state, _stateMessage);
        }
    }
}
