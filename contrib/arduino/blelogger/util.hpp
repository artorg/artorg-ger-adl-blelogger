#pragma once

#include <stdint.h>

namespace Util {
    enum class LogLevel : uint8_t {
        Debug,
        Info,
        Warning,
        Error,
        Critical
    };

    static uint16_t toUint16(const uint8_t first, const uint8_t second, const bool littleEndian) {
        return static_cast<uint16_t>(((littleEndian ? second : first) << 8) | (littleEndian ? first : second));
    }

    static uint16_t toUint16(const int first, const int second, const bool littleEndian) {
        return toUint16(static_cast<uint8_t>(first), static_cast<uint8_t>(second), littleEndian);
    }

    static void fromUint16(const uint16_t twoBytes, uint8_t& first, uint8_t& second, const bool littleEndian) {
        uint8_t msb = (twoBytes & 0xff00U) >> 8;
        uint8_t lsb = twoBytes & 0x00ffU;

        first = littleEndian ? lsb : msb;
        second = littleEndian ? msb : lsb;
    }

    static void toHex(const uint8_t byteBuf[], const uint8_t byteBufLen, char
            hexBuf[], const uint8_t hexBufLen) {
        if (hexBufLen == 2 * byteBufLen + 1) {
            const char hexChars[17] = "0123456789abcdef";
            for (uint8_t i = 0; i < byteBufLen; i++) {
                hexBuf[2 * i] = hexChars[(byteBuf[i] & 0xf0U) >> 4];
                hexBuf[2 * i + 1] = hexChars[byteBuf[i] & 0x0fU];
            }
            hexBuf[hexBufLen - 1] = 0;
        }
    }
}
