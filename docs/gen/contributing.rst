Contributing
============
If you wish to contribute to this piece of software, please make use of the
Vagrant virtualization environment. A Vagrant file is already present and you
can use it to build and test the software. If you do not know what Vagrant is,
refer to their great documentation: https://www.vagrantup.com/docs/why-vagrant/

So, when you want to work on the software, just type the following in the
project directory:

.. code-block:: shell

    $ vagrant up
    $ vagrant reload
    $ vagrant ssh
    [vagrant] $ cd /vagrant

And then continue with the build process from there. The dependencies are
already installed as part of the Vagrant provisioning process.

