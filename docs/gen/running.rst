Running
=======
The programme is designed to run as a service, but since it doesn't fork,
you can run it just like any other.

.. code-block:: shell

    $ sudo /usr/local/bin/blelogger

To start the service automatically, use systemd's interface. If you intend to
use this, ensure that the user ``blelogger`` is present on the system. For Parse
to work without errors, the account should have a home directory, but it does
not need login capabilities. Systemd uses capability bounding sets to enable the
proper permissions.

.. code-block:: shell

    $ sudo systemctl enable blelogger
    $ sudo systemctl start blelogger

