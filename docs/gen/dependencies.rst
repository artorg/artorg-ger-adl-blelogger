Dependencies
============
The project needs build tools (for C++11), cmake (at least 3.0), git, the
Bluetooth/Bluez development library, and optionally the Systemd development
library (you really don't need systemd, although keep in mind that I didn't
create an init script for the service). The parse integration further requires
the curl development library and the uuid library. The version system uses
pkg-config. On a debian derivative, you may call:

.. code-block:: shell

    $ sudo apt-get update
    $ sudo apt-get install build-essential git cmake pkg-config ninja-build
    $ sudo apt-get install bluez libbluetooth-dev libsystemd-dev libcurl4-gnutls-dev uuid-dev

On arch linux, you can use (the other dependencies are included with the base
system):

.. code-block:: shell

    $ sudo pacman -S cmake ninja bluez bluez-libs

