.. blelogger documentation master file, created by
   sphinx-quickstart on Thu Apr 28 16:36:53 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bluetooth LE Logger
===================
The BLELogger defines a device that continuously records Bluetooth LE
advertising packets to an SD card.

Contents:

.. toctree::
    :maxdepth: 2

    dependencies
    installing
    running
    configuring
    usage
    contributing
    license
    code



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

