Configuring
===========
You can configure the blelogger either at build time or later using a configuration file, which can
be found at

.. code-block:: shell

    /etc/blelogger/blelogger.ini

Instead of using the usual cmake configure command, you may change some options, for example:

.. code-block:: shell

    $ cmake -GNinja -DENABLE_ACTIVE_SCAN=ON ..

The following options are available:

* ENABLE_TESTS: Toggle unit testing. Default ON.
* ENABLE_BLUETOOTH: Toggle calls to the bluetooth library. Default ON.
* ENABLE_PARSE: Toggle calls to the Parse API. Default ON.
* ENABLE_SYSTEMD: Toggle calls to Systemd. Default ON.
* ENABLE_FILE_WRITE: Toggle dump file writing. Default ON.
* ENABLE_STRICT_ERROR: Toggle strict error handling. Default OFF.
* ENABLE_ACTIVE_SCAN: Toggle active bluetooth scanning. Default OFF.
* ENABLE_BEACON_FILTER: Toggle the beacon filtering. Default ON.

You may also want to set the Parse configuration yourself:

* PARSE_APP_ID: The string of the Parse App ID. Ex. "myAppId"
* PARSE_SERVER_URL: The URL to the Parse server including the Parse mount. Ex. "https://localhost/parse"
* PARSE_REST_URL: The URL subpath to the Parse class. Ex. "/classes/AdInfo"
* PARSE_SESSION_TOKEN: In order for the Parse logging to work, you need a valid (restricted) Parse session token.

Or modify the user under which the service runs in Systemd:

* EXECUTABLE_USER: The username that's used to run the logger. Ex. "blelogger"

