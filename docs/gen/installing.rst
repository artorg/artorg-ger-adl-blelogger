Installing
==========
After installing the dependencies, use the following sequence of commands to
build the service. By the way, by default it builds in Debug mode. I personally
prefer ninja as build-system, but you can use anything that's supported by
CMake.

.. code-block:: shell

    $ git clone https://bitbucket.com/eleanorey/blelogger.git
    $ mkdir -p blelogger/build
    $ cd blelogger/build
    $ cmake -GNinja -DCMAKE_BUILD_TYPE=Release ..
    $ ninja
    $ sudo ninja install

You may uninstall the service (after following the above steps) with:

.. code-block:: shell

    $ sudo ninja uninstall

