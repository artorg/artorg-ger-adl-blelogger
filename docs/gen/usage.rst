Usage
=====
Once it runs, the service collects Bluetooth LE/Smart advertisement packets
from nearby devices and sends them to a Parse server. In case something goes
wrong, the packets are stored as successive JSON strings (separated by a
newline) in the following dump file. As of yet, these packets are not resent.

.. code-block:: shell

    /etc/blelogger/blelogger.bkp

