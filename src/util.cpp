#include "util.h"

#include <iostream>
#include <iomanip>
#include <sstream>

using namespace BleLogger;

std::string Util::toHex (const std::uint8_t buf[], std::size_t bufLen, char byteDelim) {
    std::stringstream s;
    s << std::internal << std::setfill ('0') << std::hex;
    for (std::size_t i = 0; i < bufLen; i++) {
        s << std::setw (2) << static_cast<unsigned int> (buf [i]);
        if (i < bufLen - 1) {
            s << byteDelim;
        }
    }
    return s.str ();
}

std::string Util::toHex (const std::uint8_t buf[], std::size_t bufLen) {
    std::stringstream s;
    s << std::internal << std::setfill ('0') << std::hex;
    for (std::size_t i = 0; i < bufLen; i++) {
        s << std::setw (2) << static_cast<unsigned int> (buf [i]);
    }
    return s.str ();
}

std::string Util::toUuid (const std::uint8_t buf[], std::size_t bufLen, char uuidDelim) {
    if (bufLen == 16) {
        std::stringstream s;
        s << std::internal << std::setfill ('0') << std::hex;
        for (std::size_t i = 0; i < bufLen; i++) {
            if (i == 4 || i == 6 || i == 8 || i == 10) {
                s << uuidDelim;
            }
            s << std::setw (2) << static_cast<unsigned int> (buf [i]);
        }
        return s.str ();
    } else {
        std::cerr << "UUIDs have to be 16 bytes long." << std::endl;
        return "INVALID";
    }
}

std::uint16_t Util::toUint16 (const std::uint8_t firstByte, const std::uint8_t secondByte) {
    return firstByte | (static_cast<std::uint16_t> (secondByte) << 8);
}
