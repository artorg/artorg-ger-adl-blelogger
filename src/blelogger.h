#ifndef BLELOGGER_H
#define BLELOGGER_H

#include "hciapi.h"
#include "util.h"

#include "json.hpp"
#include "parse.h"

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

#include <cstdlib>
#include <cstdint>
#include <cstring>

#include <string>

/**
 * Define the core of the BLE Logger
 *
 * @file blelogger.h
 * @author Eleanore C. Young
 */

/**
 * The parent namespace of the project.
 */
namespace BleLogger {
    /**
     * All states that the state machine can take on.
     */
    enum class State {
        Idle,
        Uninitialized,
        DeviceSetup,
        ScanSetup,
        ScanEnable,
        FilterSetup,
        Listen,
        Shutdown,
        Reload,
        Error,
        Critical
    };

    /**
     * All messages that can be conveyed with a state transition.
     */
    enum class StateMessage {
        None,
        IllegalState,
        FailedOpeningHciDevice,
        FailedLESetScanParameters,
        FailedLESetScanEnable,
        FailedHciSetFilter,
        CaughtTerminationSignal,
        CaughtReloadSignal,
        CriticalError,
        Debug
    };

    /**
     * Defines the structure and methods for a state machine that records
     * Bluetooth LE advertisement packets. Its main entry point is {@link Logger::heartBeat}
     */
    class Logger {
        public:
            /**
             * Initialises only member variables.
             */
            Logger ();

            /**
             * Executes one step of the state machine. Must be called
             * repeatedly.
             *
             * @param abortProgram Set to 1 will abort the programme.
             * @param reloadConfig Set to 1 will cause the programme to reload the configuration.
             * @return true if the programme is still running, false otherwise.
             */
            bool heartBeat (volatile int& abortProgram, volatile int& reloadConfig);

            /**
             * Get the current state of the state machine.
             *
             * @return The current state
             */
            State getState ();

            /**
             * Toggle all calls to the Bluetooth library.
             */
            bool enableBluetooth;

            /**
             * Toggle all calls to the Parse library.
             */
            bool enableParse;

            /**
             * Toggle all calls to the Systemd library.
             */
            bool enableSystemd;

            /**
             * Toggle maintaining a local dump file for unsuccessful transmissions to Parse.
             */
            bool enableFileWrite;

            /**
             * If true, the state machine will go to a critical state on any error.
             */
            bool enableStrictError;

            /**
             * If true, enable active Bluetooth LE advertisement scanning.
             */
            bool enableActiveScan;

            /**
             * If true, only record iBeacon advertisement packets and ignore all
             * other.
             */
            bool enableBeaconFilter;

        private:
            /**
             * The length of a Bluetooth address is 6 bytes.
             */
            const std::size_t _bdaddrLen;

            /**
             * The BlueZ hardware device file descriptor.
             */
            int _deviceFD;

            /**
             * The current state of the state machine.
             */
            State _state;

            /**
             * The active Parse client instance.
             */
            ParseClient _parseClient;

            /**
             * The string that refers to the physical location of the device.
             */
            std::string _deviceLocation;

            /**
             * The string that contains the Parse App ID.
             */
            std::string _parseAppId;

            /**
             * The Parse client key is not used.
             *
             * @deprecated
             */
            std::string _parseClientKey;

            /**
             * The Parse server URL.
             */
            std::string _parseServerUrl;

            /**
             * The Parse mount point on the Parse server.
             */
            std::string _parseRestUrl;

            /**
             * The session token used to communicate with the Parse REST API.
             */
            std::string _parseSessionToken;

            /**
             * The threshold RSSI value above which debug output is logged.
             */
            std::int8_t _debugRssiThreshold;

            /**
             * The path to the backup file for unsuccessful transmissions.
             */
            std::string _dumpPath;

            /**
             * The path to the configuration file.
             */
            std::string _iniPath;

            /**
             * The amount of seconds to wait for the Bluetooth hardware to
             * respond.
             */
            int _awaitTimeout;

            /**
             * The type of Bluetooth LE scanning to be employed.
             */
            HciApi::ScanType _scanType;

            /**
             * The interval between scan calls.
             */
            std::uint16_t _scanInterval;

            /**
             * The duration of each scan call.
             */
            std::uint16_t _scanWindow;

            /**
             * The address type of the Bluetooth host.
             */
            HciApi::AddressType _addressType;

            /**
             * How to filter the incoming advertisements.
             */
            HciApi::FilterPolicy _filterPolicy;

            /**
             * If true, duplicate incoming advertisements will be ignored.
             */
            bool _filterDuplicates;

            /**
             * Returns the current system time as 64 bit integer.
             *
             * @return The UNIX epoch time (seconds since January 1 1970)
             */
            std::int64_t now ();

            /**
             * Saves the JSON payload to the log file in case something went
             * wrong with the transmission to Parse.
             *
             * @param client The Parse client instance
             * @param error The Parse error code
             * @param httpStatus The HTTP status code
             * @param httpResponseBody The HTTP response body
             * @param payload The JSON payload
             */
            void parseSaveCallback (ParseClient client, int error,
                    int httpStatus, const char* httpResponseBody,
                    nlohmann::json payload);

            /**
             * Check whether a particular advertising info packet contains
             * iBeacon advertisements.
             *
             * @param info The BlueZ advertising info packet
             * @return true if an iBeacon advertisement was found, false otherwise
             */
            bool isBeacon (const le_advertising_info& info);

            /**
             * Write the data from the BlueZ advertising info packet either to
             * Parse or to a backup file.
             *
             * @param info The BlueZ advertising info packet
             */
            void writeLogEntry (const le_advertising_info& info);

            /**
             * Perform a state transition.
             *
             * @param nextState The next state
             * @param msg The message to be passed on to the next state
             */
            void transition (State nextState, StateMessage msg = StateMessage::None);

            /**
             * The idle state entry action.
             */
            void idle () {};

            /**
             * The unititialized state entry action.
             */
            void uninitialized ();

            /**
             * The device setup state entry action.
             */
            void deviceSetup ();

            /**
             * The scan setup state entry action.
             */
            void scanSetup ();

            /**
             * The enable scanning state entry action.
             */
            void scanEnable ();

            /**
             * The filter setup state entry action.
             */
            void filterSetup ();

            /**
             * The listen state entry action. This performs the bulk of the
             * packet capturing work.
             */
            void listen ();

            /**
             * The shutdown state entry action. This prepares the state machine
             * for shutdown.
             */
            void shutdown ();

            /**
             * The reload state entry action. This reloads the programme
             * configuration.
             */
            void reload ();

            /**
             * The error state entry action. This performs cleanup in
             * non-critical conditions.
             */
            void error ();

            /**
             * The critical state entry action. This reacts to critical errors
             * and moves on to shut the programme down.
             */
            void critical () { transition (State::Shutdown, StateMessage::CriticalError); };

            /**
             * The illegal state entry action. This method should never be
             * called in normal operation of the state machine.
             */
            void illegalState () { transition (State::Critical, StateMessage::IllegalState); };
    };
}
#endif
