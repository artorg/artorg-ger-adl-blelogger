#include "blelogger.h"

#include <csignal>
#include <cerrno>

#include <iostream>
#include <system_error>

/**
 * This constitutes the main entry point into BLELogger.
 *
 * @file main.cpp
 * @author Eleanore C. Young
 */

namespace BleLogger {
    /**
     * If set to 1, tell the programme to shut down.
     */
    volatile std::sig_atomic_t abortProgram = 0;

    /**
     * If set to 1, tell the programme to reload the configuration.
     */
    volatile std::sig_atomic_t reloadConfig = 0;

    /**
     * This function is called when a signal is received.
     *
     * Upon SIGINT and SIGTERM, the programme terminates. Upon SIGHUP it reloads
     * the configuration file and re-initialises. Modifies the volatile
     * variables {@link #abortProgram} and {@link #reloadConfig}.
     *
     * @param signal Signal constant such as SIGINT
     */
    void signalHandler (int signal) {
        if (signal == SIGINT || signal == SIGTERM) {
            abortProgram = 1;
        } else if (signal == SIGHUP) {
            reloadConfig = 1;
        }
    }

    /**
     * This function registers to handle SIGINT, SIGTERM and SIGHUP.
     */
    void registerSigHandler () {
        struct sigaction sigIntHandler;
        sigIntHandler.sa_handler = signalHandler;
        sigemptyset (&sigIntHandler.sa_mask);
        sigIntHandler.sa_flags = SA_SIGINFO;

        if (sigaction (SIGINT, &sigIntHandler, nullptr) < 0) {
            throw std::system_error (errno, std::system_category ());
        }
        if (sigaction (SIGTERM, &sigIntHandler, nullptr) < 0) {
            throw std::system_error (errno, std::system_category ());
        }
        if (sigaction (SIGHUP, &sigIntHandler, nullptr) < 0) {
            throw std::system_error (errno, std::system_category ());
        }
    }
}

/**
 * This function runs the state machine.
 *
 * At first, it registers the signal handlers, then it enters into the main loop
 * of the state machine. {@link BleLogger::Logger::heartBeat} takes over from there.
 *
 * @return EXIT_FAILURE or EXIT_SUCCESS
 */
int main () {
    try {
        BleLogger::registerSigHandler ();
        auto logger = BleLogger::Logger ();
        auto running = true;
        while (running) {
            running = logger.heartBeat (BleLogger::abortProgram, BleLogger::reloadConfig);
        }
    } catch (std::system_error& e) {
        std::cerr << "Error: " << e.code () << " - " << e.code ().message () << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
