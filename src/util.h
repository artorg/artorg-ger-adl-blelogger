#ifndef BLELOGGER_UTIL_H
#define BLELOGGER_UTIL_H

#include <cstddef>
#include <cstdint>

#include <string>

/**
 * An assortment of utility functions for the BLE Logger.
 *
 * @file util.h
 * @author Eleanore C. Young
 */

namespace BleLogger {
    //* The utility namespace
    namespace Util {
        /**
         * Represent an arbitrary length byte buffer as a delimited string of
         * hex bytes.
         *
         * @param buf The byte buffer
         * @param bufLen The buffer length
         * @param byteDelim The character to delimit the individual bytes (usually a colon)
         * @return A delimited hex string (ex. 12:3e:45:67:e8:9b)
         */
        std::string toHex (const std::uint8_t buf[], std::size_t bufLen, char byteDelim);

        /**
         * Represent a byte buffer of an arbitrary length as a string of hex bytes.
         *
         * @param buf The byte buffer
         * @param bufLen The buffer length
         * @return A hex string (ex. 123e4567e89b)
         */
        std::string toHex (const std::uint8_t buf[], std::size_t bufLen);

        /**
         * Convert a 16 byte long buffer to the standard string representation of a
         * UUID.
         *
         * @param buf The byte buffer
         * @param bufLen The buffer length (must be 16)
         * @param uuidDelim The character to delimit UUID groups (usually a hyphen)
         * @return A UUID string (ex. 123e4567-e89b-12d3-a456-426655440000)
         */
        std::string toUuid (const std::uint8_t buf[], std::size_t bufLen, char uuidDelim = '-');

        /**
         * Converts two successive bytes into a 16 bit integer. Performs byte order
         * swapping also.
         *
         * @param firstByte The first of two successive bytes
         * @param secondByte The second of two successive bytes
         * @return The byte order swapped two byte integer
         */
        std::uint16_t toUint16 (const std::uint8_t firstByte, const std::uint8_t secondByte);
    }
}
#endif
