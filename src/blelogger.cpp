#include "blelogger.h"

#ifdef PROJECT_HAS_CONFIG_H
#include "config.h"
#else
#warning "Could not find the config header"
#endif

#ifdef SYSTEMD_FOUND
#include <systemd/sd-daemon.h>
#endif

#include "INIReader.h"

#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/stat.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <chrono>
#include <functional>
#include <future>

using namespace BleLogger;

/**
 * Overload the stream operator to convert a BleLogger::State enum to a
 * string stream.
 *
 * @param os The output string stream
 * @param s The enum member.
 * @return The output string stream
 */
std::ostream& operator<< (std::ostream& os, State s) {
    switch (s) {
        case State::Idle: return os << "Idle";
        case State::Uninitialized: return os << "Uninitialized";
        case State::DeviceSetup: return os << "DeviceSetup";
        case State::ScanSetup: return os << "ScanSetup";
        case State::ScanEnable: return os << "ScanEnable";
        case State::FilterSetup: return os << "FilterSetup";
        case State::Listen: return os << "Listen";
        case State::Shutdown: return os << "Shutdown";
        case State::Reload: return os << "Reload";
        case State::Error: return os << "Error";
        case State::Critical: return os << "Critical";
        default: return os << "INVALID";
    }
}

/**
 * Overload the stream operator to convert a BleLogger::StateMessage enum to a
 * string stream.
 *
 * @param os The output string stream
 * @param m The enum member.
 * @return The output string stream
 */
std::ostream& operator<< (std::ostream& os, StateMessage m) {
    switch (m) {
        case StateMessage::None: return os << "None";
        case StateMessage::IllegalState: return os << "IllegalState";
        case StateMessage::FailedOpeningHciDevice: return os << "FailedOpeningHciDevice";
        case StateMessage::FailedLESetScanParameters: return os << "FailedLESetScanParameters";
        case StateMessage::FailedLESetScanEnable: return os << "FailedLESetScanEnable";
        case StateMessage::FailedHciSetFilter: return os << "FailedHciSetFilter";
        case StateMessage::CaughtTerminationSignal: return os << "CaughtTerminationSignal";
        case StateMessage::CaughtReloadSignal: return os << "CaughtReloadSignal";
        case StateMessage::CriticalError: return os << "CriticalError";
        case StateMessage::Debug: return os << "Debug";
        default: return os << "INVALID";
    }
}

std::function<void (ParseClient, int, int, const char*)> ParseCallback;
extern "C" void parseCallbackWrapper (ParseClient client, int error, int httpStatus, const char* httpResponseBody) {
    ParseCallback (client, error, httpStatus, httpResponseBody);
}

Logger::Logger () :
#ifdef PROJECT_HAS_CONFIG_H
#ifdef ENABLE_BLUETOOTH
    enableBluetooth (true),
#else
    enableBluetooth (false),
#endif
#ifdef ENABLE_PARSE
    enableParse (true),
#else
    enableParse (false),
#endif
#ifdef ENABLE_SYSTEMD
    enableSystemd (true),
#else
    enableSystemd (false),
#endif
#ifdef ENABLE_FILE_WRITE
    enableFileWrite (true),
#else
    enableFileWrite (false),
#endif
#ifdef ENABLE_STRICT_ERROR
    enableStrictError (true),
#else
    enableStrictError (false),
#endif
#ifdef ENABLE_ACTIVE_SCAN
    enableActiveScan (true),
#else
    enableActiveScan (false),
#endif
#ifdef ENABLE_BEACON_FILTER
    enableBeaconFilter (true),
#else
    enableBeaconFilter (false),
#endif
    _bdaddrLen (6),
    _deviceFD (-1),
    _state (State::Uninitialized),
    _parseClient (),
    _deviceLocation (DEVICE_LOCATION),
    _parseAppId (PARSE_APP_ID),
    _parseClientKey (""),
    _parseServerUrl (PARSE_SERVER_URL),
    _parseRestUrl (PARSE_REST_URL),
    _parseSessionToken (PARSE_SESSION_TOKEN),
    _debugRssiThreshold (DEBUG_RSSI_THRESHOLD),
    _dumpPath (EXECUTABLE_DUMP_PATH),
    _iniPath (EXECUTABLE_INI_PATH),
    _awaitTimeout (BLUEZ_AWAIT_TIMEOUT),
    _scanType (BLE_SCAN_TYPE),
    _scanInterval (BLE_SCAN_INTERVAL),
    _scanWindow (BLE_SCAN_WINDOW),
    _addressType (BLE_ADDRESS_TYPE),
    _filterPolicy (BLE_FILTER_POLICY),
#ifdef BLE_FILTER_DUPLICATES
    _filterDuplicates (true)
#else
    _filterDuplicates (false)
#endif
#else
    enableBluetooth (true),
    enableParse (true),
    enableSystemd (true),
    enableFileWrite (true),
    enableStrictError (false),
    enableActiveScan (false),
    enableBeaconFilter(true),
    _deviceLocation ("unset"),
    _parseAppId ("myAppId"),
    _parseClientKey (""),
    _parseServerUrl ("https://localhost:5000/parse/"),
    _parseRestUrl ("/classes/AdInfo"),
    _parseSessionToken (""),
    _debugRssiThreshold (-70),
    _dumpPath ("blelogger.bkp"),
    _iniPath ("blelogger.ini"),
    _awaitTimeout (1000),
    _scanType (HciApi::ScanType::PassiveScan),
    _scanInterval (0x0010U),
    _scanWindow (0x0010U),
    _addressType (HciApi::AddressType::PublicIdentityAddress),
    _filterPolicy (HciApi::FilterPolicy::UndirectedAdsOnly),
    _filterDuplicates (false)
#endif
{}

bool Logger::heartBeat (volatile int& abortProgram, volatile int& reloadConfig) {
    if (abortProgram) {
        transition (State::Shutdown, StateMessage::CaughtTerminationSignal);
        abortProgram = 0;
        return true;
    } else if (reloadConfig) {
        transition (State::Reload, StateMessage::CaughtReloadSignal);
        reloadConfig = 0;
        return true;
    }
    switch (_state) {
        case State::Idle: idle (); break;
        case State::Uninitialized: uninitialized (); break;
        case State::DeviceSetup: deviceSetup (); break;
        case State::ScanSetup: scanSetup (); break;
        case State::ScanEnable: scanEnable (); break;
        case State::FilterSetup: filterSetup (); break;
        case State::Listen: listen (); break;
        case State::Shutdown: shutdown (); break;
        case State::Error: error (); break;
        case State::Critical: critical (); break;
        default: illegalState (); break;
    }
    if (_state == State::Idle) {
        return false;
    }
    return true;
}

State Logger::getState () {
    return _state;
}

std::int64_t Logger::now () {
    return std::chrono::duration_cast<std::chrono::seconds> (std::chrono::system_clock::now ().time_since_epoch ()).count ();
}

void Logger::parseSaveCallback (ParseClient client, int error, int httpStatus, const char* httpResponseBody,
        nlohmann::json payload) {
    if (error != 0 || httpStatus >= 400) {
        if (httpResponseBody != nullptr) {
            std::cerr << "From Parse: error=" << error << ", httpStatus=" << httpStatus
                << ", httpResponseBody=" << httpResponseBody << std::endl;
        } else {
            std::cerr << "From Parse: error=" << error << ", httpStatus=" << httpStatus << std::endl;
        }
        if (enableFileWrite) {
            std::ofstream ofs (_dumpPath, std::ios::out | std::ios::app);
            if (ofs.is_open ()) {
                ofs << payload.dump () << std::endl;
                ofs.close ();
            } else {
                std::cerr << "Error writing to dump file." << std::endl;
            }
        } else {
            std::cerr << "Local file store is disabled. The payload will be lost." << std::endl;
        }
    } else {
#ifndef NDEBUG
        if (httpResponseBody != nullptr) {
            std::cout << "From Parse: error=" << error << ", httpStatus=" << httpStatus
                << ", httpResponseBody=" << httpResponseBody << std::endl;
        } else {
            std::cout << "From Parse: error=" << error << ", httpStatus=" << httpStatus << std::endl;
        }
#endif
    }
}

bool Logger::isBeacon (const le_advertising_info& info) {
    if (enableBeaconFilter) {
        // Filter out any non-iBeacon packets
        // The length of iBeacon ad packets is generally 26 bytes
        // The ad type is vendor-specific (0xff)
        // The company id is always that of Apple (0x004c)
        // The detailed type is always 0x1502
        auto inc = 1;
        for (auto i = 0; i < info.length; i += inc) {
            auto adLen = info.data [i];
            inc = adLen + 1;

            if (adLen == 26) {
                auto adType = info.data [i + 1];
                if (adType == 0xffU) {
                    auto adCompany = Util::toUint16 (info.data [i + 2], info.data [i + 3]);
                    if (adCompany == static_cast<std::uint16_t> (HciApi::CompanyID::AppleInc)) {
                        auto adIdent = Util::toUint16 (info.data [i + 4], info.data [i + 5]);
                        if (adIdent == 0x1502U) {
#ifndef NDEBUG
                            auto adUUID = Util::toUuid (info.data + i + 6, 16, '-');
                            auto adMajor = Util::toUint16 (info.data [i + 22], info.data [i + 23]);
                            auto adMinor = Util::toUint16 (info.data [i + 24], info.data [i + 25]);
                            auto adTx = static_cast<std::int8_t> (info.data [i + 26]);
                            std::cout << "[iBeacon] UUID: " << adUUID << ", Major: " << std::to_string (adMajor) << ", Minor: " << std::to_string (adMinor) << ", Tx: " << std::to_string (adTx) << " dBa" << std::endl;
#endif
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    } else {
        return true;
    }
}

void Logger::writeLogEntry (const le_advertising_info& info) {
    // Construct the Parse.com JSON message
    nlohmann::json parsePayload;
    parsePayload ["location"] = _deviceLocation;
    parsePayload ["timestamp"] = now ();
    parsePayload ["discovery_type"] = info.evt_type;
    parsePayload ["address_type"] = info.bdaddr_type;
    parsePayload ["address"] = Util::toHex (info.bdaddr.b, _bdaddrLen);
    parsePayload ["data_length"] = info.length;
    parsePayload ["data"] = nlohmann::json::array ();

    // Process the data from the advertisement packets
    auto inc = 1;
    for (auto i = 0; i < info.length; i += inc) {
        auto adLen = info.data [i];
        inc = adLen + 1;

        // Add the data to the JSON message
        nlohmann::json adJson;
        adJson ["length"] = adLen;
        adJson ["type"] = info.data [i + 1];
        adJson ["data"] = Util::toHex (info.data + i + 2, adLen - 1);
        parsePayload ["data"].push_back (adJson);
    }

    // Set the signal strength field
    auto rssi = static_cast<std::int8_t> (info.data [info.length]);
    parsePayload ["rssi"] = rssi;

#ifndef NDEBUG
    // Print the debug representation.
    if (rssi > _debugRssiThreshold) {
        std::cout << "JSON: " << parsePayload.dump () << std::endl;
    }
#endif

    if (enableParse) {
        // Wrap the C++11 callback in a C-style callback for use with Parse
        ParseCallback = std::bind (
                &Logger::parseSaveCallback,
                this,
                std::placeholders::_1,
                std::placeholders::_2,
                std::placeholders::_3,
                std::placeholders::_4,
                parsePayload
                );

        // Send the request to Parse asynchronously
        std::future<void> request (std::async (parseSendRequest, _parseClient, "POST", _parseRestUrl.c_str (), parsePayload.dump ().c_str (), &parseCallbackWrapper));
    }
}

void Logger::transition (State nextState, StateMessage msg) {
    if (nextState != _state) {
#ifndef NDEBUG
        if (msg != StateMessage::None) {
            std::cout << _state << " -> " << nextState << ": " << msg << std::endl;
        } else {
            std::cout << _state << " -> " << nextState << std::endl;
        }
#endif
        std::stringstream s;
        if (msg != StateMessage::None) {
            s << "STATUS=" << nextState << ": " << msg;
        } else {
            s << "STATUS=" << nextState;
        }

        if (nextState == State::Shutdown) {
            s << "\nSTOPPING=1";
        } else if (nextState == State::Listen) {
            s << "\nREADY=1";
        }
#ifdef SYSTEMD_FOUND
        if (enableSystemd) {
            sd_notify (0, s.str ().c_str ());
        }
#endif

        _state = nextState;
    }
}

void Logger::uninitialized () {
    // Read the configuration values from the ini file if it is accessible.
    // TODO: Parse config values for ScanType, AddressType and FilterPolicy.
    struct stat buffer;
    if (stat (_iniPath.c_str (), &buffer) == 0) {
        INIReader config (_iniPath);
        enableBluetooth = config.GetBoolean ("Features", "EnableBluetooth", enableBluetooth);
        enableParse = config.GetBoolean ("Features", "EnableParse", enableParse);
        enableSystemd = config.GetBoolean ("Features", "EnableSystemd", enableSystemd);
        enableFileWrite = config.GetBoolean ("Features", "EnableFileWrite", enableFileWrite);
        enableStrictError = config.GetBoolean ("Features", "EnableStrictError", enableStrictError);
        enableActiveScan = config.GetBoolean ("Features", "EnableActiveScan", enableActiveScan);
        enableBeaconFilter = config.GetBoolean ("Features", "EnableBeaconFilter", enableBeaconFilter);
        _awaitTimeout = config.GetInteger ("Scan", "AwaitTimeout", _awaitTimeout);
        _scanInterval = config.GetInteger ("Scan", "ScanInterval", _scanInterval);
        _scanWindow = config.GetInteger ("Scan", "ScanWindow", _scanWindow);
        _filterDuplicates = config.GetBoolean ("Scan", "FilterDuplicates", _filterDuplicates);
        _debugRssiThreshold = config.GetInteger ("Output", "DebugRssiThreshold", _debugRssiThreshold);
        _dumpPath = config.Get ("Output", "DumpPath", _dumpPath);
        _deviceLocation = config.Get ("Output", "DeviceLocation", _deviceLocation);
        _parseAppId = config.Get ("Output", "ParseAppId", _parseAppId);
        _parseServerUrl = config.Get ("Output", "ParseServerUrl", _parseServerUrl);
        _parseRestUrl = config.Get ("Output", "ParseRestUrl", _parseRestUrl);
        _parseSessionToken = config.Get ("Output", "ParseSessionToken", _parseSessionToken);
    } else {
        std::cerr << "Config file does not exist or is not accessible: " << _iniPath << std::endl;
    }

#ifndef NDEBUG
    // TODO: Include a more informative debug message.
    std::cout << "Debug mode enabled." << std::endl;
    std::cout << "Writing dump file at: " << _dumpPath << std::endl;
    std::cout << "Using device location: " << _deviceLocation << std::endl;
#endif

    if (enableParse) {
        if (_parseAppId.length () > 0 && _parseServerUrl.length () > 0) {
            // Set up the parse client
            _parseClient = parseInitializeWithServerURL (_parseAppId.c_str (), _parseClientKey.c_str (), _parseServerUrl.c_str ());
            if (parseGetSessionToken (_parseClient) == nullptr && _parseSessionToken.length () > 0 && _parseSessionToken.compare ("invalid") != 0) {
                parseSetSessionToken (_parseClient, _parseSessionToken.c_str ());
            }
        } else {
            std::cerr << "Check the App ID and the server URL in the config. Something is wrong with Parse." << std::endl;
        }
    }

    transition (State::DeviceSetup);
}

void Logger::deviceSetup () {
    if (enableBluetooth) {
        // Open the device
        auto deviceID = hci_get_route (nullptr);
        _deviceFD = hci_open_dev (deviceID);
    } else {
        _deviceFD = 0;
        std::cerr << "Bluetooth integration disabled." << std::endl;
    }

    if (_deviceFD >= 0) {
        transition (State::ScanSetup);
    } else {
        transition (State::Critical, StateMessage::FailedOpeningHciDevice);
    }
}

void Logger::scanSetup () {
    int success;
    if (enableBluetooth) {
        success = hci_le_set_scan_parameters (
                _deviceFD,
                static_cast<std::uint8_t> (_scanType),
                static_cast<std::uint16_t> (_scanInterval),
                static_cast<std::uint16_t> (_scanWindow),
                static_cast<std::uint8_t> (_addressType),
                static_cast<std::uint8_t> (_filterPolicy),
                _awaitTimeout
                );
    } else {
        success = 0;
    }

    if (success >= 0) {
        transition (State::ScanEnable);
    } else {
        transition (State::Error, StateMessage::FailedLESetScanParameters);
    }
}

void Logger::scanEnable () {
    int success;
    if (enableBluetooth) {
        success = hci_le_set_scan_enable (
                _deviceFD,
                0x01U,
                static_cast<std::uint8_t> (_filterDuplicates),
                _awaitTimeout
                );
    } else {
        success = 0;
    }

    if (success >= 0) {
        transition (State::FilterSetup);
    } else {
        transition (State::Error, StateMessage::FailedLESetScanEnable);
    }
}

void Logger::filterSetup () {
    int success;
    if (enableBluetooth) {
        // Set filter settings
        struct hci_filter filt;
        hci_filter_clear (&filt);
        hci_filter_set_ptype (HCI_EVENT_PKT, &filt);
        hci_filter_set_event (EVT_LE_META_EVENT, &filt);
        success = setsockopt (_deviceFD, SOL_HCI, HCI_FILTER, &filt, sizeof (filt));
    } else {
        success = 0;
    }

    if (success >= 0) {
        transition (State::Listen);
    } else {
        transition (State::Error, StateMessage::FailedHciSetFilter);
    }
}

void Logger::listen () {
    if (enableBluetooth) {
        // Start scanning
        std::uint8_t buf [HCI_MAX_EVENT_SIZE];
        evt_le_meta_event* evt;
        le_advertising_info* info;

        if (read (_deviceFD, buf, sizeof (buf)) >= HCI_EVENT_HDR_SIZE) {
            evt = reinterpret_cast<evt_le_meta_event*> (buf + HCI_EVENT_HDR_SIZE + 1);
            if (evt != nullptr && evt->subevent == EVT_LE_ADVERTISING_REPORT) {
                void* offset = evt->data + 1;
                for (auto i = 0; i < evt->data [0]; i++) {
                    info = reinterpret_cast<le_advertising_info*> (offset);
                    if (info != nullptr) {
                        if (isBeacon (*info)) {
                            writeLogEntry (*info);
                        }
                        offset = info->data + info->length + 2;
                    }
                }
            }
        }
    } else {
        le_advertising_info info {};
        writeLogEntry (info);
    }
}

void Logger::shutdown () {
    if (enableBluetooth) {
        // Disable scanning
        hci_le_set_scan_enable (
                _deviceFD,
                0x00U,
                static_cast<std::uint8_t> (_filterDuplicates),
                _awaitTimeout
                );

        // Close the Hci device
        hci_close_dev (_deviceFD);
    }

    transition (State::Idle);
}

void Logger::reload () {
    if (enableBluetooth) {
        // Disable scanning
        hci_le_set_scan_enable (
                _deviceFD,
                0x00U,
                static_cast<std::uint8_t> (_filterDuplicates),
                _awaitTimeout
                );

        // Close the Hci device
        hci_close_dev (_deviceFD);
    }

    transition (State::Uninitialized);
}

void Logger::error () {
    int success;
    if (enableBluetooth) {
        success = hci_le_set_scan_enable (
                _deviceFD,
                0x00U,
                static_cast<std::uint8_t> (_filterDuplicates),
                _awaitTimeout
                );
    } else {
        success = 0;
    }

    if (success >= 0) {
        if (enableStrictError) {
            transition (State::Critical, StateMessage::Debug);
        } else {
            transition (State::ScanSetup);
        }
    } else {
        transition (State::Critical, StateMessage::FailedLESetScanEnable);
    }
}
