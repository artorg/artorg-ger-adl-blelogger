/*
 * config.h
 * Copyright (C) 2016 young <young@staff-202-186.eduroam.unibe.ch>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef CONFIG_H
#define CONFIG_H

#define VERSION "1.0.5"

#endif /* !CONFIG_H */
